Rails.application.routes.draw do
  root 'homepage#index'

  namespace :api do
    resources :ranks, only: [:update, :create, :show, :index, :destroy]
    resources :members, only: [:update, :create, :show, :index, :destroy]
    resources :adresses, only: [:update, :create, :show, :index, :destroy]
    resources :type_promos, only: [:update, :create, :show, :index, :destroy]
    resources :settings, only: [:update, :create, :show, :index, :destroy]
    resources :franchises, only: [:update, :create, :show, :index, :destroy]
    resources :suppliers, only: [:update, :create, :show, :index, :destroy]
    resources :groups, only: [:update, :create, :show, :index, :destroy]
    resources :categories, only: [:update, :create, :show, :index, :destroy]
    resources :ingredient_franchises, only: [:update, :create, :show, :index, :destroy]
    resources :ingredients, only: [:update, :create, :show, :index, :destroy]
    resources :code_promos, only: [:update, :create, :show, :index, :destroy]
    resources :foods, only: [:update, :create, :show, :index, :destroy]
    #resources :preparations, only: [:update, :create, :show, :index, :destroy]
    #resources :members_preparations, only: [:update, :create, :show, :index, :destroy]
    resources :deliveries, only: [:update, :create, :show, :index, :destroy]
    resources :cart_orders, only: [:update, :create, :show, :index, :destroy]
    resources :menus, only: [:update, :create, :show, :index, :destroy]
    resources :orders, only: [:update, :create, :show, :index, :destroy]
    resources :reviews, only: [:update, :create, :show, :index, :destroy]
    resources :order_statuses, only: [:update, :create, :show, :index, :destroy]

    post '/auth/login',    to: 'sessions#create'
    post '/auth/checkLogin', to: 'sessions#checkLogin'
    get '/auth/logout',   to: 'sessions#destroy'
    get '/auth/logged_in', to: 'sessions#is_logged_in?'

    get '/foods/category/:id', to: 'foods#index_by_category'
    get '/orders/status/:id', to: 'orders#index_by_status'

    post '/code_promos/apply/:code_name', to: 'code_promos#apply_code'
  end
  
  # Correction nécéssaire pour active_storage (le stockage des images)
  get '*path', to: 'homepage#index', constraints: lambda { |req|
    req.path.exclude? 'rails/active_storage'
  }

  #get '/*path' => 'homepage#index'
end
