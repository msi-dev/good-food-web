import React, { Fragment, Component, Image } from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";

const Header = tw.header`
  flex justify-between items-center
  max-w-screen-xl mx-auto
`;

const NavLinks = tw.div`inline-block`;

const NavLink = tw.a`
  text-lg my-2 lg:text-sm lg:mx-6 lg:my-0
  font-semibold tracking-wide transition duration-300
  pb-1 border-b-2 border-transparent hover:border-main-100 hocus:text-main-100
`;

const DisconnectLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-red-500 text-gray-100
  hocus:bg-red-700 hocus:text-gray-200 focus:shadow-outline
  border-b-0 cursor-pointer ml-2!
`;

const DashboardLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-gray-800 text-gray-100
  hocus:bg-gray-900 hocus:text-gray-200 focus:shadow-outline 
  border-b-0 cursor-pointer ml-2!
`;

const AccountLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-main-100 text-white
  hocus:bg-main-200 hocus:text-white focus:shadow-outline 
  border-b-0 cursor-pointer ml-2!
`;

const LogoLink = styled(NavLink)`
  ${tw`flex items-center font-black border-b-0 text-2xl! ml-0!`};

  img {
    ${tw`w-10 mr-3`}
  }
`;

const DesktopNavLinks = tw.nav`
  hidden lg:flex flex-1 justify-between items-center
`;

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: 'transparent',
    border: '0px'
  }
};

class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const collapseBreakpointCss = collapseBreakPointCssMap["lg"];
    const { isLoggedIn, member } = this.props;

    return (
      <Header className="header-light">
        <DesktopNavLinks css={collapseBreakpointCss.desktopNavLinks}>
          <LogoLink href="/">
            <img src="https://s3-symbol-logo.tradingview.com/goodfood-market--600.png" alt="logo" />
          </LogoLink>
          <NavLinks key={1}>
            <NavLink href="/">Accueil</NavLink>
            <NavLink href="/products">Nos menus</NavLink>
            {isLoggedIn ? (
              <Fragment>
                <NavLink href="/orders">Vos commandes</NavLink>
                <NavLink href="/cart">Panier</NavLink>

                <AccountLink href="/account">Mon compte</AccountLink>
                {
                  member.rank_id != 4 &&
                  <DashboardLink href="/dashboard">Dashboard</DashboardLink>
                }
                <DisconnectLink css={false && tw`rounded-full`}>Se déconnecter</DisconnectLink>
              </Fragment>) : (
              <DisconnectLink href="/auth/signin" css={false && tw`rounded-full`}>Se connecter</DisconnectLink>
            )
            }
          </NavLinks>
        </DesktopNavLinks>
      </Header>
    );
  }
};

const collapseBreakPointCssMap = {
  sm: {
    mobileNavLinks: tw`sm:hidden`,
    desktopNavLinks: tw`sm:flex`,
    mobileNavLinksContainer: tw`sm:hidden`
  },
  md: {
    mobileNavLinks: tw`md:hidden`,
    desktopNavLinks: tw`md:flex`,
    mobileNavLinksContainer: tw`md:hidden`
  },
  lg: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  },
  xl: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  }
};

export default Navbar;