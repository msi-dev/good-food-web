import React, { Fragment, Component } from "react";
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Redirect } from 'react-router-dom'

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container>
                <Content>
                    <FiveColumns>
                        <Column>
                            <ColumnHeading>Main</ColumnHeading>
                            <LinkList>
                                <LinkListItem>
                                    <Link href="#">Blog</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">FAQs</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Support</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">About Us</Link>
                                </LinkListItem>
                            </LinkList>
                        </Column>
                        <Column>
                            <ColumnHeading>Product</ColumnHeading>
                            <LinkList>
                                <LinkListItem>
                                    <Link href="#">Log In</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Personal</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Business</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Team</Link>
                                </LinkListItem>
                            </LinkList>
                        </Column>
                        <Column>
                            <ColumnHeading>Press</ColumnHeading>
                            <LinkList>
                                <LinkListItem>
                                    <Link href="#">Logos</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Events</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Stories</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Office</Link>
                                </LinkListItem>
                            </LinkList>
                        </Column>
                        <Column>
                            <ColumnHeading>Team</ColumnHeading>
                            <LinkList>
                                <LinkListItem>
                                    <Link href="#">Career</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Founders</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Culture</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Onboarding</Link>
                                </LinkListItem>
                            </LinkList>
                        </Column>
                        <Column>
                            <ColumnHeading>Legal</ColumnHeading>
                            <LinkList>
                                <LinkListItem>
                                    <Link href="#">GDPR</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Privacy Policy</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Terms of Service</Link>
                                </LinkListItem>
                                <LinkListItem>
                                    <Link href="#">Disclaimer</Link>
                                </LinkListItem>
                            </LinkList>
                        </Column>
                    </FiveColumns>
                    <Divider />
                    <ThreeColRow>
                        <LogoContainer>
                            <LogoImg src="https://s3-symbol-logo.tradingview.com/goodfood-market--600.png" />
                            <LogoText>Good Food</LogoText>
                        </LogoContainer>
                        <CopywrightNotice>&copy; 2022 Good Food. Tout droits réservés.</CopywrightNotice>
                        <SocialLinksContainer>
                            <SocialLink href="https://facebook.com">
                                <FacebookIcon />
                            </SocialLink>
                            <SocialLink href="https://twitter.com">
                                <TwitterIcon />
                            </SocialLink>
                            <SocialLink href="https://youtube.com">
                                <YoutubeIcon />
                            </SocialLink>
                        </SocialLinksContainer>
                    </ThreeColRow>
                </Content>
            </Container>
        );
    }
};

const FacebookIcon = props => (
    <svg fill="currentColor" viewBox="0 0 24 24">
        <path d="M14 13.5h2.5l1-4H14v-2c0-1.03 0-2 2-2h1.5V2.14c-.326-.043-1.557-.14-2.857-.14C11.928 2 10 3.657 10 6.7v2.8H7v4h3V22h4v-8.5z" />
    </svg>
)

const TwitterIcon = props => (
    <svg fill="currentColor" viewBox="0 0 24 24">
        <path d="M22.162 5.656a8.384 8.384 0 01-2.402.658A4.196 4.196 0 0021.6 4c-.82.488-1.719.83-2.656 1.015a4.182 4.182 0 00-7.126 3.814 11.874 11.874 0 01-8.62-4.37 4.168 4.168 0 00-.566 2.103c0 1.45.738 2.731 1.86 3.481a4.168 4.168 0 01-1.894-.523v.052a4.185 4.185 0 003.355 4.101 4.21 4.21 0 01-1.89.072A4.185 4.185 0 007.97 16.65a8.394 8.394 0 01-6.191 1.732 11.83 11.83 0 006.41 1.88c7.693 0 11.9-6.373 11.9-11.9 0-.18-.005-.362-.013-.54a8.496 8.496 0 002.087-2.165z" />
    </svg>
)

const YoutubeIcon = props => (
    <svg fill="currentColor" viewBox="0 0 24 24">
        <path d="M21.543 6.498C22 8.28 22 12 22 12s0 3.72-.457 5.502c-.254.985-.997 1.76-1.938 2.022C17.896 20 12 20 12 20s-5.893 0-7.605-.476c-.945-.266-1.687-1.04-1.938-2.022C2 15.72 2 12 2 12s0-3.72.457-5.502c.254-.985.997-1.76 1.938-2.022C6.107 4 12 4 12 4s5.896 0 7.605.476c.945.266 1.687 1.04 1.938 2.022zM10 15.5l6-3.5-6-3.5v7z" />
    </svg>
)

const Container = tw.div`relative bg-main-100 text-gray-100 -mb-8 -mx-8 px-8 py-20 lg:py-24`;
const Content = tw.div`max-w-screen-xl mx-auto relative z-10`;
const FiveColumns = tw.div`flex flex-wrap text-center sm:text-left justify-center sm:justify-start md:justify-between -mt-12`;

const Column = tw.div`px-4 sm:px-0 sm:w-1/3 md:w-auto mt-12`;

const ColumnHeading = tw.h5`uppercase font-bold`;

const LinkList = tw.ul`mt-6 text-sm font-medium`;
const LinkListItem = tw.li`mt-3`;
const Link = tw.a`border-b-2 border-transparent hocus:border-gray-100 pb-1 transition duration-300`;

const Divider = tw.div`my-16 border-b-2 border-main-200 w-full`;

const ThreeColRow = tw.div`flex flex-col md:flex-row items-center justify-between`;

const LogoContainer = tw.div`flex items-center justify-center md:justify-start`;
const LogoImg = tw.img`w-8`;
const LogoText = tw.h5`ml-2 text-xl font-black tracking-wider text-gray-100`;

const CopywrightNotice = tw.p`text-center text-sm sm:text-base mt-8 md:mt-0 font-medium text-gray-400`;

const SocialLinksContainer = tw.div`mt-8 md:mt-0 flex`;
const SocialLink = styled.a`
  ${tw`cursor-pointer p-2 rounded-full bg-gray-100 text-gray-900 hover:bg-gray-400 transition duration-300 mr-4 last:mr-0`}
  svg {
    ${tw`w-4 h-4`}
  }
`;

export default Footer;