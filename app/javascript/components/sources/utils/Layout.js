import Navbar from "../include/Navbar"
import Footer from "../include/Footer"
import React, {Component } from "react";

class Layout extends Component{
    constructor(props){
        super(props);
    }

    render(){

        const {isLoggedIn, member} = this.props;

        return(
            <>
                <Navbar isLoggedIn={isLoggedIn} member={member}/>
                    {this.props.children}
                <Footer/>
            </>
        )
    }
    
}

export default Layout;