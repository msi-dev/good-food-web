import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const setCart = (items) => {
    cookies.set("cart", items, {
        path: "/"
    });
}

export const getCart = () => {
    return cookies.get("cart");
}

export const createOrAddToCart = (item) => {
    if (cookies.get("cart") == undefined)
    {
        setCart([item]);
    }
    else
    {
        var cartJson = getCart();
        cartJson.push(item);
        setCart(cartJson);
    }
}

export const deleteCartItem = (index) => {
    var cartJson = getCart();
    console.log(cartJson);
    console.log(index);
    cartJson.splice(index, 1);
    console.log(cartJson);
    setCart(cartJson);
}

export const deleteCart = () => {
    cookies.remove("cart");
}
