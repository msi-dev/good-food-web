import React, { Component, Fragment } from 'react'
import FormPersonnal from '../../layouts/Account/FormPersonnal';
import { useParams } from 'react-router-dom';

class Account extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Fragment>
                <FormPersonnal member={this.props.member} adress={this.props.adress}/>
            </Fragment>
        );
    }
};

export default (props) => (<Account {...props} params={useParams()} />)
