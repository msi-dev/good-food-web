import React, { Component, Fragment } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../utils/Container";
import { motion } from "framer-motion";
import StatusOrder from '../../layouts/orders/StatusOrder';
import axios from 'axios';
import { format } from 'date-fns';
export default class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            loaded: false,
            openIndex: 0
        }
    }

    componentDidMount() {
        this.loadOrders();
    }

    handleClick(index) {
        this.setState({
            openIndex: index
        });
    }

    loadOrders = () => {
        axios.get(`/api/orders`)
            .then(resp => {
                if (resp.data) {
                    this.setState({
                        orders: resp.data,
                        loaded: true
                    })
                }
            })
    }

    render() {
        const { loaded, orders, openIndex } = this.state;
        console.log(orders);
        return (
            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <Column>
                            <HeaderContent>
                                <Subheading>Consulter vos commandes</Subheading>
                                <SectionHeading>Vos commandes</SectionHeading>
                            </HeaderContent>
                            <OrdersContainer>
                                {orders.map(item => 
                                    <Order key={item.id} onClick={() => {
                                        this.handleClick(item.id);
                                    }}>
                                        <TitleOrder>
                                            <TextOrder>Commande n°{item.id} - {format(new Date(item.order_date), "dd/MM/yyyy à HH:mm")} - Total de la commande : {item.total_amount} €</TextOrder>
                                            <ToggleIcon
                                                variants={{
                                                    collapsed: { rotate: 0 },
                                                    open: { rotate: -180 }
                                                }}
                                                initial="collapsed"
                                                animate={openIndex === item.id ? ("open") : ("collapsed")}
                                                transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                            >
                                                <ChevronDownIcon />
                                            </ToggleIcon>
                                        </TitleOrder>
                                        <Details
                                            variants={{
                                                open: { opacity: 1, height: "auto", marginTop: "16px" },
                                                collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === item.id ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            {
                                                item.foods.map(f =>
                                                    <Fragment>
                                                        - ({f.number_consumer}x) {f.name} : {f.price} €
                                                    </Fragment>
                                                )
                                            }
                                            <StatusOrder order_status_id={item.order_status_id}/>
                                        </Details>
                                    </Order>
                                )}
                            </OrdersContainer>
                        </Column>
                    </ContentWithPaddingXl>
                }
            </Container>
        );
    }
};

const ChevronDownIcon = props => (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" ><polyline points="6 9 12 15 18 9"></polyline></svg>
)

const Column = tw.div`flex flex-col items-center`;
const HeaderContent = tw.div``;
const Subheading = tw.h5`font-bold text-primary-500 mb-4 text-center`
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center w-full`
const SectionDescription = tw.p`mt-4 text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100 max-w-xl w-full text-center`;

const OrdersContainer = tw.dl`mt-12 max-w-4xl relative`;
const Order = tw.div`cursor-pointer select-none mt-5 px-8 sm:px-10 py-5 sm:py-4 rounded-lg text-gray-800 hover:text-gray-900 bg-gray-200 hover:bg-gray-300 transition duration-300`;
const TitleOrder = tw.dt`flex justify-between items-center`;
const TextOrder = tw.span`text-lg lg:text-xl font-semibold`;
const ToggleIcon = motion(styled.span`
  ${tw`ml-2 transition duration-300`}
  svg {
    ${tw`w-6 h-6`}
  }
`);

const Details = motion(tw.dd`pointer-events-none text-sm sm:text-base leading-relaxed`);
