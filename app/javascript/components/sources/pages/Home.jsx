import React, { Component, Fragment } from 'react'
import Hero from '../../layouts/home/Hero';
import MobileApp from '../../layouts/home/MobileApp';
import TabMenu from '../../layouts/home/TabMenu';

export default class Home extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Fragment>
                <Hero/>
                <TabMenu/>
                <MobileApp/>
            </Fragment>
        );
    }
};