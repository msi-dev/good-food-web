import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import DetailProduct from '../../../layouts/products/details/DetailProduct';
import axios from 'axios'

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            food: null,
            category: "",
            title: "",
            desc: "",
            nbOpinions: 0,
            avgStars: 0,
            url: "",
            loaded: false
        }
    }

    componentDidMount(){
        this.verifyProductExist();
    }

    verifyProductExist = () => {
        const { id } = this.props.params;
        axios.get(`/api/foods/${id}`)
        .then(resp => {
            console.log(resp);
            this.setState({
                food: resp.data,
                title: resp.data.name,
                desc: resp.data.description,
                url: resp.data.url,
                category: resp.data.category.name,
                loaded: true
            })
        })
    }

    render() {
        const {food, title, desc, category, nbOpinions, avgStars, url, loaded} = this.state;
        return (
            <Fragment>
                {
                    loaded &&
                    <DetailProduct food={food} category={category} title={title} desc={desc} nbOpinions={nbOpinions} avgStars={avgStars} image={url}/>
                }
            </Fragment>
        );
    }
};

export default (props) => (<Product {...props} params={useParams()}/>)