import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import axios from 'axios'
import { Container, ContentWithPaddingXl } from "../../utils/Container";
import AxiosHelper from '../../utils/AxiosHelpher';
import { withAlert } from 'react-alert'

const ChevronDownIcon = props => (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" ><polyline points="6 9 12 15 18 9"></polyline></svg>
)

const Column = tw.div`flex flex-col items-center`;
const HeaderContent = tw.div``;
const Subheading = tw.h5`font-bold text-primary-500 mb-4 text-center`
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center w-full`
const SectionDescription = tw.p`mt-4 text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100 max-w-xl w-full text-center`;

const OrdersContainer = tw.dl`mt-12 w-full relative`;
const Order = tw.div`mt-5 px-8 sm:px-10 py-5 sm:py-4 rounded-lg text-gray-800 hover:text-gray-900 bg-gray-200 hover:bg-gray-300 transition duration-300`;
const TitleOrder = tw.dt`flex justify-between items-center`;
const TextOrder = tw.span`text-lg lg:text-xl font-semibold`;
const ToggleIcon = motion(styled.span`
  ${tw`ml-2 transition duration-300`}
  svg {
    ${tw`w-6 h-6`}
  }
`);

const Details = motion(tw.dd`text-sm sm:text-base leading-relaxed`);
const InputContainer = tw.div`relative py-5 mt-6`;
const Label = tw.label`absolute top-0 left-0 tracking-wide font-semibold text-sm`;
const Input = tw.input``;
const FormContainer = styled.div`
  ${tw`p-10 sm:p-12 md:p-16 bg-gray-100 text-gray-700 rounded-lg relative`}
  form {
    ${tw`mt-4`}
  }
  h2 {
    ${tw`text-3xl sm:text-4xl font-bold`}
  }
  input,textarea {
    ${tw`w-full bg-transparent text-gray-700 text-base font-medium tracking-wide border-b-2 py-2 text-gray-700 hocus:border-main-100 focus:outline-none transition duration-200`};

    ::placeholder {
      ${tw`text-gray-400`}
    }
  }
`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-main-100 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-main-200 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;
const SubmitDeleteButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-red-800 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-red-700 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;

class DashboardPromo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openIndex: '',
            promos: [],
            new_name: '',
            new_pourcent: '',
            delete_promo_id: 0,
            loaded: false
        }
    }

    componentDidMount() {
        this.loadData();
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleClick(index) {
        this.setState({
            openIndex: index
        });
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const { new_name, new_pourcent } = this.state;
        let code_promo = {
            code_name: new_name,
            amount: new_pourcent,
            type_promo_id: 2 // pourcentage pour le moment
        }

        axios.post(`/api/code_promos`, code_promo)
            .then(response => {
                console.log(response);
                this.setState({ new_name: "", new_pourcent: ""});
                this.loadData();
                this.props.alert.success("Code promo ajouté")
            })
    }

    handleSubmitDelete = (event) => {
        event.preventDefault();
        const { delete_promo_id } = this.state;

        axios.delete(`/api/code_promos/${delete_promo_id}`)
            .then(response => {
                console.log(response);
                this.setState({ delete_promo_id: 0});
                this.loadData();
                this.props.alert.success("Code promo supprimé")
            })
    }

    loadData = () => {
        axios.get(`/api/code_promos`)
        .then(resp => {
            this.setState({
                promos: resp.data.data,
                loaded: true
            })
        })
    }

    render() {
        const { openIndex,  promos, loaded, delete_promo_id, new_name, new_pourcent} = this.state;
        return (

            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <Column>
                            <HeaderContent>
                                <Subheading>Gestion des codes promos</Subheading>
                                <SectionHeading>Dashboard</SectionHeading>
                                <SectionDescription>Vous pouvez ajouter / supprimer des codes promos</SectionDescription>
                            </HeaderContent>
                            <OrdersContainer>
                                <Order onClick={() => {
                                    this.handleClick("add");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Ajouter un code promo</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "add" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "add" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitNew}>
                                            <FormContainer>
                                                <InputContainer>
                                                    <Label htmlFor="name">Libellé du code</Label>
                                                    <Input id="name" type="text" name="new_name" placeholder="Le libellé du nouveau plat" onChange={this.handleChange} value={new_name} />
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="desc">Pourcentage</Label>
                                                    <Input id="desc" type="text" name="new_pourcent" placeholder="Description" onChange={this.handleChange} value={new_pourcent} />
                                                </InputContainer>
                                                <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                                <Order onClick={() => {
                                    this.handleClick("delete");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Supprimer un code promo</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitDelete}>
                                            <FormContainer>

                                                <InputContainer>
                                                    <Label htmlFor="category">Code promo</Label>
                                                    <Select value={delete_promo_id} name="delete_promo_id" id="delete_promo_id" onChange={this.handleChange}>
                                                        <option value={0}>Choisir un code promo</option>
                                                        {promos.map(p =>
                                                            <option key={p.id} value={p.id}>{p.attributes.code_name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                <SubmitDeleteButton type="submit" value="Submit">Supprimer</SubmitDeleteButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                            </OrdersContainer>
                        </Column>
                    </ContentWithPaddingXl>
                }
            </Container >
        );
    }
};
export default withAlert()(DashboardPromo);