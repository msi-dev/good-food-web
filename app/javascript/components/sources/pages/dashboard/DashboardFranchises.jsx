import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import axios from 'axios'
import { Container, ContentWithPaddingXl } from "../../utils/Container";
import AxiosHelper from '../../utils/AxiosHelpher';
import { withAlert } from 'react-alert'

const ChevronDownIcon = props => (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" ><polyline points="6 9 12 15 18 9"></polyline></svg>
)

const Column = tw.div`flex flex-col items-center`;
const HeaderContent = tw.div``;
const Subheading = tw.h5`font-bold text-primary-500 mb-4 text-center`
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center w-full`
const SectionDescription = tw.p`mt-4 text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100 max-w-xl w-full text-center`;

const OrdersContainer = tw.dl`mt-12 w-full relative`;
const Order = tw.div`mt-5 px-8 sm:px-10 py-5 sm:py-4 rounded-lg text-gray-800 hover:text-gray-900 bg-gray-200 hover:bg-gray-300 transition duration-300`;
const TitleOrder = tw.dt`flex justify-between items-center`;
const TextOrder = tw.span`text-lg lg:text-xl font-semibold`;
const ToggleIcon = motion(styled.span`
  ${tw`ml-2 transition duration-300`}
  svg {
    ${tw`w-6 h-6`}
  }
`);

const Details = motion(tw.dd`text-sm sm:text-base leading-relaxed`);
const InputContainer = tw.div`relative py-5 mt-6`;
const Label = tw.label`absolute top-0 left-0 tracking-wide font-semibold text-sm`;
const Input = tw.input``;
const FormContainer = styled.div`
  ${tw`p-10 sm:p-12 md:p-16 bg-gray-100 text-gray-700 rounded-lg relative`}
  form {
    ${tw`mt-4`}
  }
  h2 {
    ${tw`text-3xl sm:text-4xl font-bold`}
  }
  input,textarea {
    ${tw`w-full bg-transparent text-gray-700 text-base font-medium tracking-wide border-b-2 py-2 text-gray-700 hocus:border-main-100 focus:outline-none transition duration-200`};

    ::placeholder {
      ${tw`text-gray-400`}
    }
  }
`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-main-100 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-main-200 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;
const SubmitDeleteButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-red-800 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-red-700 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;

class DashboardFranchises extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openIndex: '',
            franchises: [],
            loaded: false,
            new_name: '',
            new_street: '',
            new_street_number: 0,
            new_city: '',
            new_postcode: '',
            new_country: '',
            modify_street: '',
            modify_street_number: 0,
            modify_city: '',
            modify_postcode: '',
            modify_country: '',
            showModifyInfo: false,
            modify_franchise_id: 0,
            delete_franchise_id: 0
        }
    }

    componentDidMount() {
        this.loadData();
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleClick(index) {
        this.setState({
            openIndex: index
        });
    }

    loadData = () => {
        axios.get('/api/franchises')
            .then(resp => {
                this.setState({
                    franchises: resp.data,
                    loaded: true
                });
            })
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const { new_name, new_city, new_country, new_postcode, new_street, new_street_number } = this.state;

        let adress = {
            street: new_street,
            street_number: new_street_number,
            city: new_city,
            postcode: new_postcode,
            country: new_country,
        }

        let franchise = {
            name: new_name,
            adress: adress,
            group_id: 1
        }

        axios.post(`/api/franchises`, franchise)
            .then(response => {
                this.setState({ new_name: "", new_city: "", new_country: "", new_postcode: "", new_street: "" , new_street_number: ""});
                this.props.alert.success("Franchise ajoutée")
                this.loadData();
            })
    }

    handleSubmitModify = (event) => {
        event.preventDefault();

        const { modify_name, modify_city, modify_country, modify_postcode, modify_street, modify_street_number , modify_franchise_id} = this.state;

        let adress = {
            street: modify_street,
            street_number: modify_street_number,
            city: modify_city,
            postcode: modify_postcode,
            country: modify_country,
        }

        let franchise = {
            name: modify_name,
            adress: adress,
            group_id: 1
        }

        axios.patch(`/api/franchises/${modify_franchise_id}`, franchise)
            .then(response => {
                this.setState({ modify_franchise_id: 0, showModifyInfo: false});
                this.props.alert.success("Franchise modifée")
                this.loadData();
            })
    }

    handleSubmitDelete = (event) => {
        event.preventDefault();
        const { delete_franchise_id } = this.state;

        if (delete_franchise_id != 0) {
            axios.delete(`/api/franchises/${delete_franchise_id}`)
                .then(resp => {
                    this.setState({ delete_franchise_id: 0 });
                    this.props.alert.success("Franchise supprimée")
                    this.loadData();
                })
        }
    }

    handleChooseFranchise = (event) => {
        console.log(event.target.value)
        if (event.target.value === 0) {
            this.setState({ showModifyInfo: false });
        } else {
            axios.get(`/api/franchises/${event.target.value}`)
                .then(response => {
                    this.setState({ showModifyInfo: true, modify_franchise_id: response.data.id, modify_name: response.data.name, modify_street_number: response.data.adress.street_number, modify_street: response.data.adress.street, modify_postcode: response.data.adress.postcode, modify_city: response.data.adress.city, modify_country: response.data.adress.country });
                })
        }
    }


    render() {
        const { openIndex, new_name, new_city, new_country, new_postcode, new_street, new_street_number, franchises, loaded, delete_franchise_id, showModifyInfo, modify_city, modify_name, modify_country, modify_franchise_id, modify_postcode, modify_street, modify_street_number } = this.state;
        return (

            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <Column>
                            <HeaderContent>
                                <Subheading>Gestion des franchises</Subheading>
                                <SectionHeading>Dashboard</SectionHeading>
                                <SectionDescription>Vous pouvez ajouter / modifier / supprimer des franchises</SectionDescription>
                            </HeaderContent>
                            <OrdersContainer>
                                <Order onClick={() => {
                                    this.handleClick("add");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Ajouter une franchise</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "add" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "add" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitNew}>
                                            <FormContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_name">Nom de la franchise</Label>
                                                    <Input id="new_name" type="text" name="new_name" placeholder="Le nom de la franchise" onChange={this.handleChange} value={new_name}/>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_street_number">Numéro de rue de l'adresse</Label>
                                                    <Input id="new_street_number" type="text" name="new_street_number" placeholder="Numéro de rue de l'adresse" onChange={this.handleChange} value={new_street_number}/>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_street">Rue de l'adresse</Label>
                                                    <Input id="new_street" type="numeric" name="new_street" placeholder="Rue de l'adresse" onChange={this.handleChange} value={new_street}/>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_city">Ville de l'adresse</Label>
                                                    <Input id="new_city" type="numeric" name="new_city" placeholder="Ville de l'adresse" onChange={this.handleChange} value={new_city}/>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_postcode">Code postal de l'adresse</Label>
                                                    <Input id="new_postcode" type="numeric" name="new_postcode" placeholder="Code postal de l'adresse" onChange={this.handleChange} value={new_postcode}/>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_country">Pays de l'adresse</Label>
                                                    <Input id="new_country" type="numeric" name="new_country" placeholder="Pays de l'adresse" onChange={this.handleChange} value={new_country}/>
                                                </InputContainer>
                                                <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                                <Order onClick={() => {
                                    this.handleClick("modify");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Modifier une franchise</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "modify" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "modify" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitModify}>
                                            <FormContainer>
                                                <InputContainer>
                                                    <Label htmlFor="category">Franchise</Label>
                                                    <Select value={modify_franchise_id} name="modify_franchise_id" id="modify_franchise_id" onChange={this.handleChooseFranchise}>
                                                        <option value={0}>Choisir une franchise</option>
                                                        {franchises.map(f =>
                                                            <option key={f.id} value={f.id}>{f.name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                {
                                                    showModifyInfo &&
                                                    <Fragment>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_name">Nom de la franchise</Label>
                                                            <Input id="modify_name" type="text" name="modify_name" placeholder="Le nom de la franchise" onChange={this.handleChange} value={modify_name}/>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_street_number">Numéro de rue de l'adresse</Label>
                                                            <Input id="modify_street_number" type="text" name="modify_street_number" placeholder="Numéro de rue de l'adresse" onChange={this.handleChange} value={modify_street_number}/>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_street">Rue de l'adresse</Label>
                                                            <Input id="modify_street" type="numeric" name="modify_street" placeholder="Rue de l'adresse" onChange={this.handleChange} value={modify_street}/>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_city">Ville de l'adresse</Label>
                                                            <Input id="modify_city" type="numeric" name="modify_city" placeholder="Ville de l'adresse" onChange={this.handleChange} value={modify_city}/>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_postcode">Code postal de l'adresse</Label>
                                                            <Input id="modify_postcode" type="numeric" name="modify_postcode" placeholder="Code postal de l'adresse" onChange={this.handleChange} value={modify_postcode}/>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_country">Pays de l'adresse</Label>
                                                            <Input id="modify_country" type="numeric" name="modify_country" placeholder="Pays de l'adresse" onChange={this.handleChange} value={modify_country}/>
                                                        </InputContainer>
                                                        <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                                    </Fragment>
                                                }
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                                <Order onClick={() => {
                                    this.handleClick("delete");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Supprimer une franchise</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitDelete}>
                                            <FormContainer>

                                                <InputContainer>
                                                    <Label htmlFor="category">Franchise</Label>
                                                    <Select value={delete_franchise_id} name="delete_franchise_id" id="delete_franchise_id" onChange={this.handleChange}>
                                                        <option value={0}>Choisir une franchise</option>
                                                        {franchises.map(f =>
                                                            <option key={f.id} value={f.id}>{f.name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                <SubmitDeleteButton type="submit" value="Submit">Supprimer</SubmitDeleteButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                            </OrdersContainer>
                        </Column>
                    </ContentWithPaddingXl>
                }
            </Container >
        );
    }
};
export default withAlert()(DashboardFranchises);