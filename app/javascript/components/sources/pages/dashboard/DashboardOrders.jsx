import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import axios from 'axios'
import { Container, ContentWithPaddingXl } from "../../utils/Container";
import { withAlert } from 'react-alert'

const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;
const CardContainer = tw.div`mt-10 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 sm:pr-10 md:pr-6 lg:pr-12`;
const Card = tw(motion.a)`bg-gray-200 rounded-b block max-w-xs mx-auto sm:max-w-none sm:mx-0`;
const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;
const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-main-100! text-gray-100!`}
  }
`;
const CardText = tw.div`p-4 text-gray-900 text-center`;
const CardTitle = tw.h5`text-lg font-semibold group-hover:text-main-200 text-center`;
const CardContent = tw.p`mt-1 text-sm font-medium text-gray-600`;
const CardPerm = tw.p`mt-4 font-bold text-primary-500 mb-4 text-center`;
const Button = tw.button`px-8 py-3 text-sm font-bold rounded bg-main-100 text-gray-100 hocus:bg-main-200 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;

const TabContent = tw(motion.div)`flex flex-wrap sm:-mr-10 md:-mr-6 lg:-mr-12`;
class DashboardOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: [],
            orders: [],
            activeTab: 1,
            loaded: true
        }
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = () => {
        axios.get(`/api/order_statuses`)
            .then(resp => {
                if (resp.data) {
                    this.setState({
                        status: resp.data,
                        activeTab: resp.data[0].id
                    })
                }
            })

        this.loadOrders(this.state.activeTab);
    }

    handleChangeTab = (id) => {
        this.setState({ activeTab: id, orders: [] });
        this.loadOrders(id);
    }

    loadOrders = (id) => {
        axios.get(`/api/orders/status/${id}`)
            .then(resp => {
                if (resp.data) {

                    this.setState({
                        orders: resp.data
                    })
                }
            })
    }

    handleValidateOrder = (order_id, status_id) => {
        let order = {
            order_status_id: status_id+1
        }
        console.log(order);

        axios.patch(`/api/orders/${order_id}`, order)
        .then(resp => {
            if(resp){
                this.loadOrders();
                this.props.alert.success("Statut modifié")
            }
        })
    }

    render() {
        const { loaded, orders, status, activeTab } = this.state;
        return (
            <Container>
                {
                    loaded &&
                    <ContentWithPaddingXl>
                        <HeaderRow>
                            <TabsControl>
                                {status.map(s =>
                                    <TabControl onClick={() => this.handleChangeTab(s.id)} key={s.id} active={s.id === activeTab}>
                                        {s.name}
                                    </TabControl>
                                )}
                            </TabsControl>
                        </HeaderRow>
                        <TabContent>
                            {
                                orders.map(o =>
                                    <CardContainer key={o.id}>
                                        <Card>
                                            <CardText>
                                                <CardTitle>Commande n°{o.id}</CardTitle>
                                                {
                                                    o.foods.map(f =>
                                                        <CardContent key={f.id}>({f.number_consumer}x) {f.name} : {f.price} €</CardContent>
                                                    )
                                                }
                                                <CardPerm>Prix total : {o.total_amount} €</CardPerm>
                                                {
                                                    o.order_status_id < 3 &&
                                                    <Button onClick={() => this.handleValidateOrder(o.id, o.order_status_id)}>Valider</Button>
                                                }
                                            </CardText>
                                        </Card>
                                    </CardContainer>
                                )
                            }
                        </TabContent>
                    </ContentWithPaddingXl>
                }

            </Container>
        );
    }
};
export default withAlert()(DashboardOrders);