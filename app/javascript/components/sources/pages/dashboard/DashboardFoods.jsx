import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import axios from 'axios'
import { Container, ContentWithPaddingXl } from "../../utils/Container";
import AxiosHelper from '../../utils/AxiosHelpher';
import { withAlert } from 'react-alert'

const ChevronDownIcon = props => (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" ><polyline points="6 9 12 15 18 9"></polyline></svg>
)

const Column = tw.div`flex flex-col items-center`;
const HeaderContent = tw.div``;
const Subheading = tw.h5`font-bold text-primary-500 mb-4 text-center`
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center w-full`
const SectionDescription = tw.p`mt-4 text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100 max-w-xl w-full text-center`;

const OrdersContainer = tw.dl`mt-12 w-full relative`;
const Order = tw.div`mt-5 px-8 sm:px-10 py-5 sm:py-4 rounded-lg text-gray-800 hover:text-gray-900 bg-gray-200 hover:bg-gray-300 transition duration-300`;
const TitleOrder = tw.dt`flex justify-between items-center`;
const TextOrder = tw.span`text-lg lg:text-xl font-semibold`;
const ToggleIcon = motion(styled.span`
  ${tw`ml-2 transition duration-300`}
  svg {
    ${tw`w-6 h-6`}
  }
`);

const Details = motion(tw.dd`text-sm sm:text-base leading-relaxed`);
const InputContainer = tw.div`relative py-5 mt-6`;
const Label = tw.label`absolute top-0 left-0 tracking-wide font-semibold text-sm`;
const Input = tw.input``;
const FormContainer = styled.div`
  ${tw`p-10 sm:p-12 md:p-16 bg-gray-100 text-gray-700 rounded-lg relative`}
  form {
    ${tw`mt-4`}
  }
  h2 {
    ${tw`text-3xl sm:text-4xl font-bold`}
  }
  input,textarea {
    ${tw`w-full bg-transparent text-gray-700 text-base font-medium tracking-wide border-b-2 py-2 text-gray-700 hocus:border-main-100 focus:outline-none transition duration-200`};

    ::placeholder {
      ${tw`text-gray-400`}
    }
  }
`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-main-100 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-main-200 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;
const SubmitDeleteButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-red-800 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-red-700 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;

class DashboardFoods extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openIndex: '',
            categories: [],
            foods: [],
            imageNew: null,
            loaded: false,
            new_name: '',
            new_price: '',
            new_desc: '',
            new_category_id: 0,
            delete_food_id: 0,
            showModifyInfo: false,
            modify_food_id: 0,
            modify_name: '',
            modify_desc: '',
            modify_price: '',
            modify_category_id: 0,
            imageModify: null
        }
    }

    componentDidMount() {
        this.loadData();
    }

    onNewFileChange = event => {
        this.setState({ imageNew: event.target.files[0] });
    };

    onModifyFileChange = event => {
        this.setState({ imageModify: event.target.files[0] });
    };

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleClick(index) {
        this.setState({
            openIndex: index
        });
    }

    loadData = () => {
        axios.get('/api/categories')
            .then(resp => {
                this.setState({
                    categories: resp.data.data,
                });
            })

        axios.get('/api/foods')
            .then(resp => {
                this.setState({
                    foods: resp.data.data,
                    loaded: true
                });
            })
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const { new_name, new_desc, new_price, new_category_id, imageNew } = this.state;
        var formData = new FormData();
        if (imageNew != null)
            formData.append('image', imageNew);
        formData.append('name', new_name);
        formData.append('description', new_desc);
        formData.append('price', new_price);
        formData.append('category_id', new_category_id);
        console.log(formData);

        axios.post(`/api/foods`, formData)
            .then(response => {
                console.log(response);
                this.setState({ new_name: "", new_desc: "", new_price: 0, new_category_id: 0, imageNew: null });
                this.props.alert.success("Plat ajouté")
                this.loadData();
            })
    }

    handleSubmitModify = (event) => {
        event.preventDefault();
        const { modify_food_id, modify_name, modify_desc, modify_price, modify_category_id, imageModify } = this.state;
        var formData = new FormData();
        if (imageModify != null)
            formData.append('image', imageModify);
        
        formData.append('name', modify_name);
        formData.append('description', modify_desc);
        formData.append('price', modify_price);
        formData.append('category_id', modify_category_id);
        console.log(formData);
        AxiosHelper();
        axios.post(`/api/foods`, formData)
            .then(response => {
                console.log(response);
                this.setState({ modify_name: "", modify_desc: "", modify_price: 0, modify_category_id: 0, imageModify: null });
            })

        let food = {
            is_archived: 1,
            is_active: 0
        }

        axios.patch(`/api/foods/${modify_food_id}`, food)
            .then(resp => {
                console.log(resp)
                this.props.alert.success("Plat modifié")
            })


        this.loadData();
    }

    handleSubmitDelete = (event) => {
        event.preventDefault();
        const { delete_food_id } = this.state;

        if (delete_food_id != 0) {
            axios.delete(`/api/foods/${delete_food_id}`)
                .then(resp => {
                    console.log(resp)
                    this.setState({ delete_food_id: 0 });
                    this.props.alert.success("Plat supprimé")
                })
        }
    }

    handleChooseFood = (event) => {
        if (event.target.value === 0) {
            this.setState({ showModifyInfo: false });
        } else {
            axios.get(`/api/foods/${event.target.value}`)
                .then(response => {
                    this.setState({ showModifyInfo: true, modify_food_id: response.data.data.id, modify_name: response.data.data.attributes.name, modify_desc: response.data.data.attributes.description, modify_price: response.data.data.attributes.price, modify_category_id: response.data.data.attributes.category_id });
                })
        }
    }


    render() {
        const { openIndex, new_category_id, categories, foods, loaded, delete_food_id, showModifyInfo, modify_food_id, modify_name, modify_desc, modify_price, modify_category_id } = this.state;
        return (

            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <Column>
                            <HeaderContent>
                                <Subheading>Gestion des plats</Subheading>
                                <SectionHeading>Dashboard</SectionHeading>
                                <SectionDescription>Vous pouvez ajouter / modifier / supprimer des plats</SectionDescription>
                            </HeaderContent>
                            <OrdersContainer>
                                <Order onClick={() => {
                                    this.handleClick("add");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Ajouter un plat</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "add" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "add" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitNew}>
                                            <FormContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_name">Libellé du plat</Label>
                                                    <Input id="new_name" type="text" name="new_name" placeholder="Le libellé du nouveau plat" onChange={this.handleChange} />
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="desc">Description</Label>
                                                    <Input id="desc" type="text" name="new_desc" placeholder="Description" onChange={this.handleChange} />
                                                </InputContainer><InputContainer>
                                                    <Label htmlFor="price">Montant en €</Label>
                                                    <Input id="price" type="numeric" name="new_price" placeholder="Prix du plat" onChange={this.handleChange} />
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="new_category_id">Catégorie</Label>
                                                    <Select value={new_category_id} name="new_category_id" id="new_category_id" onChange={this.handleChange}>
                                                        <option value={0}>Choisir une catégorie</option>
                                                        {categories.map(c =>
                                                            <option key={c.id} value={c.id}>{c.attributes.name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                <InputContainer>
                                                    <Label htmlFor="picture">Photo du plat</Label>
                                                    <Input id="picture" type="file" accept="image/png, image/jpeg" name="picture" onChange={this.onNewFileChange} />
                                                </InputContainer>
                                                <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                                <Order onClick={() => {
                                    this.handleClick("modify");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Modifier un plat</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "modify" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "modify" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitModify}>
                                            <FormContainer>
                                                <InputContainer>
                                                    <Label htmlFor="category">Plat</Label>
                                                    <Select value={modify_food_id} name="modify_food_id" id="modify_food_id" onChange={this.handleChooseFood}>
                                                        <option value={0}>Choisir un plat</option>
                                                        {foods.map(f =>
                                                            <option key={f.id} value={f.id}>{f.attributes.name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                {
                                                    showModifyInfo &&
                                                    <Fragment>
                                                        <InputContainer>
                                                            <Label htmlFor="name">Libellé du plat</Label>
                                                            <Input id="name" type="text" name="modify_name" placeholder="Le libellé du nouveau plat" onChange={this.handleChange} value={modify_name} />
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="desc">Description</Label>
                                                            <Input id="desc" type="text" name="new_desc" placeholder="Description" onChange={this.handleChange} value={modify_desc} />
                                                        </InputContainer><InputContainer>
                                                            <Label htmlFor="price">Montant en €</Label>
                                                            <Input id="price" type="numeric" name="new_price" placeholder="Prix du plat" onChange={this.handleChange} value={modify_price} />
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="modify_category_id">Catégorie</Label>
                                                            <Select value={modify_category_id} name="modify_category_id" id="modify_category_id" onChange={this.handleChange}>
                                                                <option value={0}>Choisir une catégorie</option>
                                                                {categories.map(c =>
                                                                    <option key={c.id} value={c.id}>{c.attributes.name}</option>
                                                                )}
                                                            </Select>
                                                        </InputContainer>
                                                        <InputContainer>
                                                            <Label htmlFor="picture">Photo du plat</Label>
                                                            <Input id="picture" type="file" accept="image/png, image/jpeg" name="picture" onChange={this.onModifyFileChange} />
                                                        </InputContainer>
                                                        <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                                    </Fragment>
                                                }
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                                <Order onClick={() => {
                                    this.handleClick("delete");
                                }}>
                                    <TitleOrder>
                                        <TextOrder>Supprimer un plat</TextOrder>
                                        <ToggleIcon
                                            variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                            }}
                                            initial="collapsed"
                                            animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                            transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            <ChevronDownIcon />
                                        </ToggleIcon>
                                    </TitleOrder>
                                    <Details
                                        variants={{
                                            open: { opacity: 1, height: "auto", marginTop: "16px" },
                                            collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                        }}
                                        initial="collapsed"
                                        animate={openIndex === "delete" ? ("open") : ("collapsed")}
                                        transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                    >
                                        <form onSubmit={this.handleSubmitDelete}>
                                            <FormContainer>

                                                <InputContainer>
                                                    <Label htmlFor="category">Plat</Label>
                                                    <Select value={delete_food_id} name="delete_food_id" id="delete_food_id" onChange={this.handleChange}>
                                                        <option value={0}>Choisir un plat</option>
                                                        {foods.map(f =>
                                                            <option key={f.id} value={f.id}>{f.attributes.name}</option>
                                                        )}
                                                    </Select>
                                                </InputContainer>
                                                <SubmitDeleteButton type="submit" value="Submit">Supprimer</SubmitDeleteButton>
                                            </FormContainer>
                                        </form>
                                    </Details>
                                </Order>
                            </OrdersContainer>
                        </Column>
                    </ContentWithPaddingXl>
                }
            </Container >
        );
    }
};
export default withAlert()(DashboardFoods);