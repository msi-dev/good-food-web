import React, { Component, Fragment } from 'react'
import DashboardMenu from '../../layouts/dashboard/DashboardMenu';

export default class Dashboard extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <DashboardMenu/>
        );
    }
};