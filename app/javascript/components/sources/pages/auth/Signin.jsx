import React, { Component, Fragment } from 'react'
import { Container as ContainerBase } from "../../utils/Container";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import illustration from "../../../../assets/images/signup-illustration.svg";
import axios from 'axios'

export default class Signin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    createSession = (member) => {
        axios.post('/api/auth/login', { member }, { withCredentials: true })
            .then(response => {
                this.props.handleLogin(response.data)
            })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { email, password } = this.state;

        let member = {
            email: email,
            password: password
        }

        axios.post('/api/auth/checkLogin', { member }, { withCredentials: true })
            .then(resp => {
                console.log("ok")
                if (resp.data.logged_in) {
                    this.createSession(member);
                }
            }).catch(error => console.log(error))
    }

    render() {
        return (
            <Container>
                <Content>
                    <MainContainer>
                        <LogoLink href="">
                            <LogoImage src="https://s3-symbol-logo.tradingview.com/goodfood-market--600.png" />
                        </LogoLink>
                        <MainContent>
                            <Heading>Connexion</Heading>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmit}>
                                    <Input name='email' type="email" onChange={this.handleChange} placeholder="Adresse e-mail" />
                                    <Input name='password' type="password" onChange={this.handleChange} placeholder="Mot de passe" />
                                    <SubmitButton type="submit">
                                        <span className="text">Se connecter</span>
                                    </SubmitButton>

                                    <p tw="mt-8 text-sm text-gray-600 text-center">
                                        Vous n'avez pas encore de compte ?{" "}
                                        <a href="/auth/signup" tw="border-b border-gray-500 border-dotted">
                                            S'inscrire
                                        </a>
                                    </p>
                                </Form>
                            </FormContainer>
                        </MainContent>
                    </MainContainer>
                    <IllustrationContainer>
                        <IllustrationImage imageSrc={illustration} />
                    </IllustrationContainer>
                </Content>
            </Container>
        );
    }
};

const Container = tw(ContainerBase)`min-h-screen bg-main-100 text-white font-medium flex justify-center -m-8`;
const Content = tw.div`max-w-screen-xl m-0 sm:mx-20 sm:my-16 bg-white text-gray-900 shadow sm:rounded-lg flex justify-center flex-1`;
const MainContainer = tw.div`lg:w-1/2 xl:w-5/12 p-6 sm:p-12`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold`;
const FormContainer = tw.div`w-full flex-1 mt-8`;

const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-main-100 text-gray-100 w-full py-4 rounded-lg hover:bg-main-200 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
  .icon {
    ${tw`w-6 h-6 -ml-2`}
  }
  .text {
    ${tw`ml-3`}
  }
`;
const IllustrationContainer = tw.div`sm:rounded-r-lg flex-1 bg-purple-100 text-center hidden lg:flex justify-center`;
const IllustrationImage = styled.div`
  ${props => `background-image: url("${props.imageSrc}");`}
  ${tw`m-12 xl:m-16 w-full max-w-lg bg-contain bg-center bg-no-repeat`}
`;