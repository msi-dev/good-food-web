import React, { Component, Fragment } from 'react'
import styled, { ThemeConsumer } from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import CartItem from '../../layouts/cart/CartItem';
import { getCart, deleteCart } from "../../sources/utils/CartHelper";
import axios from 'axios'
import { withAlert } from 'react-alert'

const Container = tw.div`relative`;
const TwoColumn = tw.div`flex flex-col lg:flex-row max-w-screen-xl mx-auto py-20 md:py-24`;
const TabContent = tw(motion.div)`flex flex-wrap sm:-mr-10 md:-mr-6 lg:-mr-12`;
const LeftColumn = tw.div`relative lg:mt-0 lg:w-9/12 lg:pr-12 flex-shrink-0 lg:text-left`;
const RightColumn = tw.div`relative mt-12 lg:mt-0 flex flex-col`;
const Heading = tw.h1`text-blue-900 font-black text-3xl md:text-5xl leading-snug max-w-3xl`;

const PromoContainer = tw.div`w-full mt-5`;
const Promo = tw(motion.div)`bg-gray-200 block max-w-xs mx-auto sm:max-w-none sm:mx-0`;
const PromoInput = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const PromoButton = tw.button`w-full font-bold py-4 rounded bg-gray-700 text-gray-100 hocus:bg-gray-800 focus:shadow-outline focus:outline-none transition duration-300`;
const SubHeading = tw.h2`text-gray-600 lg:mx-5`
const TwoColumnPromo = tw.div`flex flex-col lg:flex-row max-w-screen-xl mx-auto py-5`;
const LeftColumnPromo = tw.div`relative lg:mx-5 lg:w-6/12 flex-shrink-0 lg:text-left`;
const RightColumnPromo = tw.div`relative lg:w-3/12 mt-12 lg:mt-0 flex flex-col`;

const TotalContainer = tw.div`w-full mt-5`;
const Total = tw(motion.div)`bg-gray-200 block max-w-xs mx-auto sm:max-w-none sm:mx-0`;
const TotalButton = tw.button`font-bold w-full py-4 lg:text-center rounded bg-main-100 text-gray-100 hocus:bg-main-200 focus:shadow-outline focus:outline-none transition duration-300`;
const TotalButtonDiv = tw.div`relative justify-center`;

const PriceContainer = tw.div``;
const ContentContainer = tw.div`border-b-2 border-main-100`;

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foods: [],
            totalFood: 0,
            code_promo: "",
            code_promo_id: 0,
            promotion: 0
        }
    }

    componentDidMount(){
        this.getCartElements();
    }

    getCartElements = () => {
        var items = getCart();
        
        if (items == undefined || items.length == 0)
            return;
        
        var amount = 0
        items.forEach(f => {
            amount += Number.parseFloat(f.price);
        });

        this.setState({
            foods: getCart(),
            totalFood: Math.round(amount * 100) / 100
        })
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    applyCodePromo = () => {
        const { code_promo, foods, totalFood } = this.state;

        if (foods == undefined || foods.length == 0)
        {
            return;
        }

        if (code_promo.trim != "")
        {
            axios.post(`/api/code_promos/apply/${code_promo}`, { total_amount: totalFood })
            .then(response => {
                this.setState({
                    promotion: Math.round(Number.parseFloat(response.data.remise) * 100) / 100,
                    code_promo_id: Number.parseInt(response.data.code_promo_id)
                });
                this.props.alert.success("Code promo appliqué")
            }).catch(err => {
                this.props.alert.error("Code promo invalide")
            })
        }
    }

    submitOrder = (total_amount) => {
        const { foods, code_promo_id, promotion } = this.state;

        if (foods == undefined || foods.length == 0)
        {
            return;
        }

        let order = {
            total_amount: total_amount,
            code_promo_id: promotion == 0 ? code_promo_id : null,
            member_id: this.props.member.id,
            foods: foods
        }

        axios.post(`/api/orders/`, {order})
        .then(response => {
            //this.setState({ a: 0 });
            deleteCart();
            this.props.alert.error("Commande envoyé")
            window.location.reload();
        })
    }

    render() {
        const { foods, totalFood, code_promo, promotion } = this.state;

        var count = 0;
        
        if (foods != undefined)
        {
            count = foods.length;
        }
        
        var total = Math.round((totalFood - promotion) * 100) / 100;

        return (
            <Container>
                <TwoColumn>
                    <LeftColumn>
                        <Heading>Votre panier</Heading>
                        <TabContent>
                            {foods.map((f, index) =>
                                <CartItem key={index} food={f} title={f.name} quantity="1" />
                            )}
                        </TabContent>
                    </LeftColumn>
                    <RightColumn>
                        <Heading>Récapitulatif</Heading>
                        <PromoContainer>
                            <Promo>
                                <TwoColumnPromo>
                                    <LeftColumnPromo>
                                        <PromoInput onChange={this.handleChange} name="code_promo" placeholder='Saisir le code' type="text" value={code_promo}/>
                                    </LeftColumnPromo>
                                    <RightColumnPromo>
                                        <PromoButton onClick={() => this.applyCodePromo()}>OK</PromoButton>
                                    </RightColumnPromo>
                                </TwoColumnPromo>
                            </Promo>
                        </PromoContainer>
                        <TotalContainer>
                            <Total>
                                <ContentContainer>
                                    <TwoColumnPromo>
                                        <LeftColumnPromo>
                                            <SubHeading>Panier ({count}) :</SubHeading>
                                        </LeftColumnPromo>
                                        <RightColumnPromo>
                                            <SubHeading>{totalFood} €</SubHeading>
                                        </RightColumnPromo>
                                    </TwoColumnPromo>
                                </ContentContainer>
                                <ContentContainer>
                                    <TwoColumnPromo>
                                        <LeftColumnPromo>
                                            <SubHeading>Promotion :</SubHeading>
                                        </LeftColumnPromo>
                                        <RightColumnPromo>
                                            <SubHeading>{promotion} €</SubHeading>
                                        </RightColumnPromo>
                                    </TwoColumnPromo>
                                </ContentContainer>
                                <PriceContainer>
                                    <TwoColumnPromo>
                                        <LeftColumnPromo>
                                            <SubHeading>Total à payer :</SubHeading>
                                        </LeftColumnPromo>
                                        <RightColumnPromo>
                                            <SubHeading>{total} €</SubHeading>
                                        </RightColumnPromo>
                                    </TwoColumnPromo>
                                </PriceContainer>
                                <TotalButtonDiv>
                                    <TotalButton onClick={() => this.submitOrder(total)}>Valider la commande</TotalButton>
                                </TotalButtonDiv>
                            </Total>
                        </TotalContainer>
                    </RightColumn>
                </TwoColumn>
            </Container>
        );
    }
};
export default withAlert()(Cart);

