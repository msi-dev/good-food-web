import React, { Component } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import {Redirect}  from 'react-router-dom'

const Container = tw.div`relative`;
const TwoColumn = tw.div`flex flex-col lg:flex-row md:items-center max-w-screen-xl mx-auto py-20 md:py-24`;
const LeftColumn = tw.div`relative lg:w-6/12 lg:pr-12 flex-shrink-0 text-center lg:text-left`;
const RightColumn = tw.div`relative mt-12 lg:mt-0 flex flex-col justify-center`;

const Heading = tw.h1`text-blue-900 font-black text-3xl md:text-5xl leading-snug max-w-3xl`;
const Paragraph = tw.p`my-5 lg:my-8 text-sm lg:text-base font-medium text-gray-600 max-w-lg mx-auto lg:mx-0`;

const Actions = tw.div`flex flex-col items-center sm:flex-row justify-center lg:justify-start mt-8`;
const PrimaryButton = tw.button`font-bold px-8 lg:px-10 py-3 rounded bg-main-100 text-gray-100 hocus:bg-main-200 focus:shadow-outline focus:outline-none transition duration-300`;
const IllustrationContainer = tw.div`flex justify-center md:justify-end items-center relative max-w-3xl lg:max-w-none`;
const imageCss = tw`rounded-4xl`;
const HighlightedText = tw.span`bg-main-100 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class Hero extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Container>
                <TwoColumn>
                    <LeftColumn>
                        <Heading>Titre <HighlightedText>Miam !</HighlightedText></Heading>
                        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Paragraph>
                        <Actions>
                            <PrimaryButton>Commander maintenant !</PrimaryButton>
                        </Actions>
                    </LeftColumn>
                    <RightColumn>
                        <IllustrationContainer>
                        <img
                            css={imageCss}
                            src="https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=768&q=80"
                            alt="Hero"
                        />
                        </IllustrationContainer>
                    </RightColumn>
                </TwoColumn>
            </Container>
        )
    }
}

export default Hero;