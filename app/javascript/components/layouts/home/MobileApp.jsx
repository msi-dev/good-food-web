import React, { Component } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Redirect } from 'react-router-dom'
import appleIconImageSrc from "../../../assets/images/apple-icon.png";
import googlePlayIconImageSrc from "../../../assets/images/google-play-icon.png"
import mockupImageSrc from "../../../assets/images/app-mockup.png"
import { Container as ContainerBase, ContentWithPaddingXl } from "../../sources/utils/Container";

const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center`
const SubheadingBase = tw.h5`font-bold text-primary-500`

const Container = tw(ContainerBase)`bg-gray-900 -mx-8`
const Content = tw(ContentWithPaddingXl)``
const Row = tw.div`px-8 flex items-center relative z-10 flex-col lg:flex-row text-center lg:text-left justify-center`;

const ColumnContainer = tw.div`max-w-2xl`
const TextContainer = tw(ColumnContainer)``;
const Text = tw(SectionHeading)`text-gray-100 lg:text-left max-w-none text-3xl leading-snug`;
const Subheading = tw(SubheadingBase)`text-yellow-500 mb-4 tracking-wider`

const LinksContainer = tw.div`mt-8 lg:mt-16 flex flex-col items-center sm:block`
const Link = styled.a`
  ${tw`w-56 p-3 sm:p-4 text-sm sm:text-base font-bold uppercase tracking-wider rounded-full inline-flex justify-center items-center mt-6 first:mt-0 sm:mt-0 sm:ml-8 first:ml-0 bg-gray-100 hocus:bg-gray-300 text-gray-900 hocus:text-gray-900 shadow hover:shadow-lg focus:shadow-outline focus:outline-none transition duration-300`}
  img {
    ${tw`inline-block h-8 mr-3`}
  }
  span {
    ${tw`leading-none inline-block`}
  }
`;

const ImageContainer = tw(ColumnContainer)`mt-16 lg:mt-0 lg:ml-16 flex justify-end`;

class MobileApp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container css={tw`mb-20 lg:mb-24`}>
                <Content>
                    <Row>
                        <TextContainer>
                            <Subheading>Télécharger l'application</Subheading>
                            <Text>Texte</Text>
                            <LinksContainer>
                                <Link href="">
                                    <img src={appleIconImageSrc} alt="" />
                                    <span>App Store</span>
                                </Link>
                                <Link href="">
                                    <img src={googlePlayIconImageSrc} alt="" />
                                    <span>Google Play</span>
                                </Link>
                            </LinksContainer>
                        </TextContainer>
                        <ImageContainer>
                            <img src={mockupImageSrc} alt="" tw="w-64" />
                        </ImageContainer>
                    </Row>
                </Content>
            </Container>
        )
    }
}

export default MobileApp;