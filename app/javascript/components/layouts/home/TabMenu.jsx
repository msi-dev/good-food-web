import React, { Component } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../sources/utils/Container";
import { motion } from "framer-motion";
import { getCart, createOrAddToCart} from "../../sources/utils/CartHelper";
import axios from 'axios'
import { withAlert } from 'react-alert'

const StarIcon = props => (
    <svg viewBox="0 0 1792 1792">
        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1385 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T365 1569q0-6 2-20l86-500L89 695q-25-27-25-48 0-37 56-46l502-73L847 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z" />
    </svg>
)
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center`
const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)`text-blue-900`;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;
const PrimaryButtonBase = tw.button`px-8 py-3 font-bold rounded bg-main-100 text-gray-100 hocus:bg-main-200 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-main-100! text-gray-100!`}
  }
`;

const TabContent = tw(motion.div)`mt-6 flex flex-wrap sm:-mr-10 md:-mr-6 lg:-mr-12`;
const CardContainer = tw.div`mt-10 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 sm:pr-10 md:pr-6 lg:pr-12`;
const Card = tw(motion.a)`bg-gray-200 rounded-b block max-w-xs mx-auto sm:max-w-none sm:mx-0`;
const CardImageContainer = styled.div`
  ${props => css`background-image: url("${props.imageSrc}");`}
  ${tw`h-56 xl:h-64 bg-center bg-cover relative rounded-t`}
`;
const CardRatingContainer = tw.div`leading-none absolute inline-flex bg-gray-100 bottom-0 left-0 ml-4 mb-4 rounded-full px-5 py-2 items-end`;
const CardRating = styled.div`
  ${tw`mr-1 text-sm font-bold flex items-end`}
  svg {
    ${tw`w-4 h-4 fill-current text-orange-400 mr-1`}
  }
`;

const CardHoverOverlay = styled(motion.div)`
  background-color: rgba(255, 255, 255, 0.5);
  ${tw`absolute inset-0 flex justify-center items-center`}
`;
const CardButton = tw(PrimaryButtonBase)`text-sm`;

const CardReview = tw.div`font-medium text-xs text-gray-600`;

const CardText = tw.div`p-4 text-gray-900`;
const CardTitle = tw.h5`text-lg font-semibold group-hover:text-main-200`;
const CardContent = tw.p`mt-1 text-sm font-medium text-gray-600`;
const CardPrice = tw.p`mt-4 text-xl font-bold`;

const HighlightedText = tw.span`bg-main-100 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class TabMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTabId: 0,
            categories: [],
            foods: []
        }
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = () => {
        axios.get(`/api/categories`)
            .then(resp => {
                this.setState({
                    categories: resp.data.data,
                    activeTabId: resp.data.data[0].id
                })
                this.loadFoods(resp.data.data[0].id);
            })
    }

    handleClickTab = (category_id) => {
        this.setState({
            activeTabId: category_id
        });
        this.loadFoods(category_id);
    }

    loadFoods = (category_id) => {
        axios.get(`/api/foods/category/${category_id}`)
            .then(resp => {
                this.setState({
                    foods: resp.data
                })
            })
    }

    addToCart = (food) => {
        //console.log(food);
        createOrAddToCart(food);
        this.props.alert.success("Plat ajouté au panier")
        //console.log(getCart());
    }

    render() {
        const { categories, activeTabId, foods } = this.state;

        return (
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>La <HighlightedText>faim</HighlightedText> qui vous convient !</Header>
                        <TabsControl>
                            {categories.map(c =>
                                <TabControl key={c.id} active={c.id === activeTabId} onClick={() => this.handleClickTab(c.id)}>
                                    {c.attributes.name}
                                </TabControl>
                            )}
                        </TabsControl>
                    </HeaderRow>
                    <TabContent>
                        {
                            foods.map(f =>
                                <CardContainer key={f.id}>
                                    <Card className="group" initial="rest" whileHover="hover" animate="rest">
                                        <CardImageContainer imageSrc={f.url}>
                                            <CardRatingContainer>
                                                <CardRating>
                                                    <StarIcon />
                                                    X
                                                </CardRating>
                                                <CardReview>(Aucun avis)</CardReview>
                                            </CardRatingContainer>
                                            <CardHoverOverlay
                                                variants={{
                                                    hover: {
                                                        opacity: 1,
                                                        height: "auto"
                                                    },
                                                    rest: {
                                                        opacity: 0,
                                                        height: 0
                                                    }
                                                }}
                                                transition={{ duration: 0.3 }}
                                            >
                                                <CardButton onClick={() => this.addToCart(f)}>Ajouter</CardButton>
                                            </CardHoverOverlay>
                                        </CardImageContainer>
                                        <CardText>
                                            <CardTitle>{f.name}</CardTitle>
                                            <CardContent>{f.description}</CardContent>
                                            <CardPrice>{f.price} €</CardPrice>
                                        </CardText>
                                    </Card>
                                </CardContainer>
                            )
                        }
                    </TabContent>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(TabMenu);