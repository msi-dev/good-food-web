import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";

class DashboardMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container>
                <Content>
                    <TabContent>
                        <CardContainer>
                            <Card className="group" href="/dashboard/orders" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des commandes</CardTitle>
                                    <CardContent>Voir les commandes</CardContent>
                                    <CardPerm>Accès employé</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                        <CardContainer>
                            <Card className="group" href="/dashboard/foods" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des plats</CardTitle>
                                    <CardContent>Ajouter / Modifier / Supprimer</CardContent>
                                    <CardPerm>Accès gérant</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                        <CardContainer>
                            <Card className="group" href="/dashboard/code_promos" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des codes promos</CardTitle>
                                    <CardContent>Ajouter / Supprimer</CardContent>
                                    <CardPerm>Accès gérant</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                        <CardContainer>
                            <Card className="group" href="/dashboard/suppliers" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des fournisseurs</CardTitle>
                                    <CardContent>Ajouter / Modifier / Supprimer</CardContent>
                                    <CardPerm>Accès gérant</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                        <CardContainer>
                            <Card className="group" href="/dashboard/members" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des utilisateurs</CardTitle>
                                    <CardContent>Ajouter / Modifier / Supprimer</CardContent>
                                    <CardPerm>Accès administrateur</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                        <CardContainer>
                            <Card className="group" href="/dashboard/franchises" initial="rest" whileHover="hover" animate="rest">
                                <CardText>
                                    <CardTitle>Gestion des franchises</CardTitle>
                                    <CardContent>Ajouter / Modifier / Supprimer</CardContent>
                                    <CardPerm>Accès administrateur</CardPerm>
                                </CardText>
                            </Card>
                        </CardContainer>
                    </TabContent>
                </Content>
            </Container>
        );
    }
};

export default (props) => (<DashboardMenu {...props} params={useParams()} />)
const Container = tw.div`relative`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;
const CardContainer = tw.div`mt-10 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 sm:pr-10 md:pr-6 lg:pr-12`;
const Card = tw(motion.a)`bg-gray-200 rounded-b block max-w-xs mx-auto sm:max-w-none sm:mx-0`;

const CardText = tw.div`p-4 text-gray-900 text-center`;
const CardTitle = tw.h5`text-lg font-semibold group-hover:text-main-200 text-center`;
const CardContent = tw.p`mt-1 text-sm font-medium text-gray-600`;
const CardPerm = tw.p`mt-4 font-bold text-primary-500 mb-4 text-center`;

const TabContent = tw(motion.div)`flex flex-wrap sm:-mr-10 md:-mr-6 lg:-mr-12`;