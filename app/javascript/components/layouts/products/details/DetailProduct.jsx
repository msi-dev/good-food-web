import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { getCart, createOrAddToCart} from "../../../sources/utils/CartHelper";
import { withAlert } from 'react-alert'

const PrimaryButtonBase = tw.button`px-8 py-3 font-bold rounded bg-main-100 text-gray-100 hocus:bg-main-200 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;
const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center`
const SubheadingBase = tw.h5`font-bold text-main-100`

const Container = tw.div`relative`;
const TwoColumn = tw.div`flex flex-col md:flex-row justify-between max-w-screen-xl mx-auto py-20 md:py-24`;
const Column = tw.div`w-full max-w-md mx-auto md:max-w-none md:mx-0`;
const ImageColumn = tw(Column)`md:w-5/12 flex-shrink-0 h-80 md:h-auto relative`;
const TextColumn = styled(Column)(props => [
    tw`md:w-7/12 mt-16 md:mt-0`,
    props.textOnLeft ? tw`md:mr-12 lg:mr-16 md:order-first` : tw`md:ml-12 lg:ml-16 md:order-last`
]);

const Image = styled.div(props => [
    `background-image: url("${props.imageSrc}");`,
    tw`rounded bg-contain bg-no-repeat bg-center h-full`
]);
const TextContent = tw.div`lg:py-8 text-center md:text-left`;

const Subheading = tw(SubheadingBase)`text-center md:text-left`;
const Heading = tw(
    SectionHeading
)`mt-4 font-black text-left text-3xl sm:text-4xl lg:text-5xl text-center md:text-left leading-tight`;
const Description = tw.p`mt-4 text-center md:text-left text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100`;

const Statistics = tw.div`flex flex-col items-center sm:block text-center md:text-left mt-4`;
const Statistic = tw.div`text-left sm:inline-block sm:mr-12 last:mr-0 mt-4`;
const Value = tw.div`font-bold text-lg sm:text-xl lg:text-2xl text-secondary-500 tracking-wide`;
const Key = tw.div`font-medium text-primary-700`;

const PrimaryButton = tw(PrimaryButtonBase)`mt-8 md:mt-10 text-sm inline-block mx-auto md:mx-0`;
class DetailProduct extends Component {
    constructor(props) {
        super(props);
    }

    addToCart = (food) => {
        //console.log(food);
        createOrAddToCart(food);
        //console.log(getCart());
        this.props.alert.success("Plat ajouté au panier")
   };


    render() {
        return (
            <Container>
                <TwoColumn css={tw`md:items-center`}>
                    <ImageColumn>
                        <img src={this.props.image} css={tw`bg-cover`} alt="" />
                    </ImageColumn>
                    <TextColumn textOnLeft={true}>
                        <TextContent>
                            <Subheading>{this.props.category}</Subheading>
                            <Heading>{this.props.title}</Heading>
                            <Description>{this.props.desc}</Description>
                            <Statistics>
                                <Statistic>
                                    <Value>{this.props.nbOpinions}</Value>
                                    <Key>Avis postés</Key>
                                </Statistic>
                                <Statistic>
                                    <Value>{this.props.avgStars}/5</Value>
                                    <Key>Note</Key>
                                </Statistic>
                            </Statistics>
                            <PrimaryButton onClick={() => this.addToCart(this.props.food)}>
                                Ajouter au panier
                            </PrimaryButton>
                        </TextContent>
                    </TextColumn>
                </TwoColumn>
            </Container>
        );
    }
};

const page = (props) => (<DetailProduct {...props} params={useParams()} />)

export default withAlert()(page);