import React, { Component, Fragment } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../sources/utils/Container";
import { motion } from "framer-motion";
import { useParams } from 'react-router-dom';
import chefIcon from "../../../assets/images/chef-icon.svg"

class StatusOrder extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { order_status_id } = this.props;
    console.log(this.props);
    return (
      <Container>
        <ContentWithPaddingXl>
          <ThreeColumnContainer>
            <Column>
              <Card>
                <span className="imageContainer" css={tw`p-2!`}>
                  <img src={chefIcon} alt="" css={tw`w-20! h-20!`} />
                </span>
                <span className="title">En préparation</span>
                <p className="description">Nos cuisiniers mettent tout en oeuvre pour réaliser votre commande.</p>
                {
                  order_status_id == 1 &&
                  <span className="link">
                    <span>En cours</span>
                  </span>
                }

              </Card>
            </Column>
            <Column>
              <Card>
                <span className="imageContainer" css={tw`p-2!`}>
                  <img src={chefIcon} alt="" css={tw`w-20! h-20!`} />
                </span>
                <span className="title">Livraison en cours</span>
                <p className="description">Notre livreur est en chemin vers votre adresse.</p>
                {
                  order_status_id == 2 &&
                  <span className="link">
                    <span>En cours</span>
                  </span>
                }
              </Card>
            </Column>
            <Column>
              <Card>
                <span className="imageContainer" css={tw`p-2!`}>
                  <img src={chefIcon} alt="" css={tw`w-20! h-20!`} />
                </span>
                <span className="title">Commande livrée</span>
                <p className="description">N'hésiter pas à laisser un avis sur les plats de votre commande.</p>
                {
                  order_status_id == 3 &&
                  <span className="link">
                    <span>En cours</span>
                  </span>
                }
              </Card>
            </Column>
          </ThreeColumnContainer>
        </ContentWithPaddingXl>
      </Container>
    );
  }
};

export default (props) => (<StatusOrder {...props} params={useParams()} />)

const ThreeColumnContainer = styled.div`
  ${tw`flex flex-col items-center lg:items-stretch lg:flex-row flex-wrap lg:justify-center max-w-screen-lg mx-auto`}
`;
const Column = styled.div`
  ${tw`lg:w-1/3 max-w-xs`}
`;
const Card = styled.a`
  ${tw`flex flex-col items-center text-center h-full mx-4 px-4 py-8 rounded transition-transform duration-300 hover:cursor-pointer transform hover:scale-105 `}
  .imageContainer {
    ${tw`text-center rounded-full p-4 bg-gray-100`}
    img {
      ${tw`w-8 h-8`}
    }
  }

  .title {
    ${tw`mt-4 font-bold text-xl leading-none`}
  }

  .description {
    ${tw`mt-4 text-sm font-medium text-secondary-300`}
  }

  .link {
    ${tw`mt-auto inline-flex items-center pt-5 text-sm font-bold text-primary-300 leading-none hocus:text-primary-900 transition duration-300`}
    .icon {
      ${tw`ml-2 w-4`}
    }
  }
`;