import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import { deleteCartItem } from "../../sources/utils/CartHelper";
import { withAlert } from 'react-alert'

const CardContainer = tw.div`mt-10 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 sm:pr-10 md:pr-6 lg:pr-12`;
const Card = tw(motion.a)`bg-gray-200 rounded-b block max-w-xs mx-auto sm:max-w-none sm:mx-0`;
const PrimaryButtonBase = tw.button`px-8 py-3 font-bold rounded bg-red-700 text-gray-100 hocus:bg-red-800 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;
const CardImageContainer = styled.div`
  ${props => css`background-image: url("${props.imageSrc}");`}
  ${tw`h-56 xl:h-64 bg-center bg-cover relative rounded-t`}
`;
const CardRatingContainer = tw.div`leading-none absolute inline-flex bg-gray-100 bottom-0 left-0 ml-4 mb-4 rounded-full px-5 py-2 items-end`;
const CardRating = styled.div`
  ${tw`mr-1 text-sm font-bold flex items-end`}
  svg {
    ${tw`w-4 h-4 fill-current text-orange-400 mr-1`}
  }
`;

const CardHoverOverlay = styled(motion.div)`
  background-color: rgba(255, 255, 255, 0.5);
  ${tw`absolute inset-0 flex justify-center items-center`}
`;
const CardButton = tw(PrimaryButtonBase)`text-sm`;

const CardReview = tw.div`font-medium text-xs text-gray-600`;

const CardText = tw.div`p-4 text-gray-900`;
const CardTitle = tw.h5`text-lg font-semibold group-hover:text-main-200`;
const CardContent = tw.p`mt-1 text-sm font-medium text-gray-600`;
const CardPrice = tw.p`mt-4 text-xl font-bold`;
const StarIcon = props => (
    <svg viewBox="0 0 1792 1792">
        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1385 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T365 1569q0-6 2-20l86-500L89 695q-25-27-25-48 0-37 56-46l502-73L847 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z" />
    </svg>
)
class CartItem extends Component {
    constructor(props) {
        super(props);
    }

    deleteCartItem = (index) => {
        deleteCartItem(index);
        //console.log(getCart());
        this.props.alert.success("Plat retiré du panier")
        window.location.reload();
    }

    render() {
        return (
            <CardContainer>
                <Card className="group" initial="rest" whileHover="hover" animate="rest">
                    <CardImageContainer imageSrc={this.props.food.url}>
                        <CardRatingContainer>
                            <CardRating>
                                <StarIcon />
                                5
                            </CardRating>
                            <CardReview>(2 avis)</CardReview>
                        </CardRatingContainer>
                        <CardHoverOverlay
                            variants={{
                                hover: {
                                    opacity: 1,
                                    height: "auto"
                                },
                                rest: {
                                    opacity: 0,
                                    height: 0
                                }
                            }}
                            transition={{ duration: 0.3 }}
                        >
                            <CardButton onClick={() => this.deleteCartItem(this.props.key)}>Retirer</CardButton>
                        </CardHoverOverlay>
                    </CardImageContainer>
                    <CardText>
                        <CardTitle>{this.props.title}</CardTitle>
                        <CardContent>{this.props.food.description}</CardContent>
                        <CardPrice>{this.props.food.price} €</CardPrice>
                    </CardText>
                </Card>
            </CardContainer>
        );
    }
};
const page = (props) => (<CartItem {...props} params={useParams()} />)

export default withAlert()(page);