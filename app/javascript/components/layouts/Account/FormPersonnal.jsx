import React, { Component, Fragment } from 'react'
import { useParams } from 'react-router-dom';
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { motion } from "framer-motion";
import axios from 'axios'
import { withAlert } from 'react-alert'

const Container = tw.div`relative`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;

const FormContainer = styled.div`
  ${tw`p-10 sm:p-12 md:p-16 bg-gray-100 text-gray-700 rounded-lg relative`}
  form {
    ${tw`mt-4`}
  }
  h2 {
    ${tw`text-3xl sm:text-4xl font-bold`}
  }
  input,textarea {
    ${tw`w-full bg-transparent text-gray-700 text-base font-medium tracking-wide border-b-2 py-2 text-gray-700 hocus:border-main-100 focus:outline-none transition duration-200`};

    ::placeholder {
      ${tw`text-gray-400`}
    }
  }
`;

const TwoColumn = tw.div`flex flex-col sm:flex-row justify-between`;
const Column = tw.div`sm:w-5/12 flex flex-col`;
const InputContainer = tw.div`relative py-5 mt-6`;
const Label = tw.label`absolute top-0 left-0 tracking-wide font-semibold text-sm`;
const Input = tw.input``;
const TextArea = tw.textarea`h-24 sm:h-full resize-none`;
const SubmitButton = tw.button`w-full sm:w-32 mt-6 py-3 bg-main-100 text-gray-100 rounded-full font-bold tracking-wide shadow-lg uppercase text-sm transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-main-200 hover:text-gray-200 hocus:-translate-y-px hocus:shadow-xl`;

class FormPersonnal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            street: '',
            street_number: 0,
            city: '',
            postcode: '',
            country: '',
            password: '',
            newPassword: '',
            loaded: false
        }
    }

    componentDidMount = () => {
        this.loadAccount();
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleSubmitMember = (event) => {
        event.preventDefault();
        const { firstname, lastname, email, phone, street, street_number, city, postcode, country } = this.state;
        let adress = {
            street: street,
            street_number: street_number,
            city: city,
            postcode: postcode,
            country: country,
        }

        if (street.trim() == '' || street_number <= 0 || city.trim() == '' || (postcode.trim() == '' || postcode.length != 5) || country.trim() == '')
        {
            adress = null
        }

        let member = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            phone: phone,
            adress: adress
        }

        axios.patch(`/api/members/${this.props.member.id}`, { member })
            .then(response => {
                if (!response.error) {
                    console.log("Compte modifié")
                    this.props.alert.success("Compte modifié")
                }
            })
    }

    handleSubmitPassword = (event) => {
        event.preventDefault();
        const { password, newPassword } = this.state;

        if (password === newPassword) {
            let member = {
                password: password
            }

            axios.patch(`/api/members/${this.props.member.id}`, { member })
                .then(response => {
                    if (!response.error) {
                        console.log("Mot de passe modifié")
                        this.props.alert.success("Mot de passe modifié")
                    }
                })
        }
    }

    loadAccount = () => {
        const { member, adress } = this.props;

        this.setState({
            firstname: member.firstname,
            lastname: member.lastname,
            email: member.email,
            phone: member.phone ?? '',
            loaded: true,
            street: adress?.street ?? '',
            street_number: adress?.street_number ?? 0,
            city: adress?.city ?? '',
            postcode: adress?.postcode ?? '',
            country: adress?.country ?? ''
        })
    }

    render() {
        const { loaded, firstname, lastname, email, phone, street, street_number, city, country, postcode } = this.state;
        return (
            loaded &&
            <Container>
                <Content>
                    <FormContainer>
                        <div tw="mx-auto max-w-4xl">
                            <h2>Gestion de votre compte</h2>
                            <form onSubmit={this.handleSubmitMember}>
                                <TwoColumn>
                                    <Column>
                                        <InputContainer>
                                            <Label htmlFor="name-input">Prénom</Label>
                                            <Input id="name-input" type="text" name="firstname" onChange={this.handleChange} placeholder="Votre prénom" value={firstname} />
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="lastname-input">Nom de famille</Label>
                                            <Input id="lastname-input" type="text" name="lastname" onChange={this.handleChange} placeholder="Votre nom de famille" value={lastname} />
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="email-input">Adresse e-mail</Label>
                                            <Input id="email-input" type="email" name="email" onChange={this.handleChange} placeholder="Votre adresse e-mail" value={email} />
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="phone-input">Numéro de téléphone (optionnel)</Label>
                                            <Input maxLength={10} id="phone-input" type="text" onChange={this.handleChange} name="phone" placeholder="Votre numéro de téléphone" value={phone} />
                                        </InputContainer>
                                        <SubmitButton type="submit" value="Submit">Valider</SubmitButton>
                                    </Column>
                                    <Column>
                                        <InputContainer>
                                            <Label htmlFor="adress-num-input">Adresse : numéro de rue (optionnel)</Label>
                                            <Input id="adress-num-input" type="text" name="street_number" placeholder="Num Rue" onChange={this.handleChange} value={street_number}/>
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="adress-input">Adresse : rue (optionnel)</Label>
                                            <Input id="adress-input" type="text" name="street" placeholder="Rue" onChange={this.handleChange} value={street}/>
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="adress-cp-input">Adresse : Code postal (optionnel)</Label>
                                            <Input id="adress-cp-input" type="text" name="postcode" placeholder="Code postal" pattern="[0-9]+" maxLength={5} onChange={this.handleChange} value={postcode}/>
                                        </InputContainer><InputContainer>
                                            <Label htmlFor="adress-city-input">Adresse : Ville (optionnel)</Label>
                                            <Input id="adress-city-input" type="text" name="city" placeholder="Ville" onChange={this.handleChange} value={city}/>
                                        </InputContainer>
                                        <InputContainer>
                                            <Label htmlFor="adress-country-input">Adresse : Pays (optionnel)</Label>
                                            <Input id="adress-country-input" type="text" name="country" placeholder="Pays" onChange={this.handleChange}  value={country}/>
                                        </InputContainer>
                                    </Column>
                                </TwoColumn>
                            </form>
                            <form onSubmit={this.handleSubmitPassword}>
                                <TwoColumn>
                                    <Column>
                                        <InputContainer>
                                            <Label htmlFor="password-input">Nouveau mot de passe</Label>
                                            <Input id="password-input" type="password" name="password" onChange={this.handleChange} placeholder="Votre ancien mot de passe" />
                                        </InputContainer>
                                        <SubmitButton type="submit" value="Submit">Modifier</SubmitButton>
                                    </Column>
                                    <Column>
                                        <InputContainer>
                                            <Label htmlFor="new-password-input">Confirmer mot de passe</Label>
                                            <Input id="new-password-input" type="password" name="newPassword" onChange={this.handleChange} placeholder="Votre nouveau mot de passe" />
                                        </InputContainer>
                                    </Column>

                                </TwoColumn>
                            </form>
                        </div>
                    </FormContainer>
                </Content>
            </Container >
        );
    }
};

const page = (props) => (<FormPersonnal {...props} params={useParams()} />)

export default withAlert()(page);