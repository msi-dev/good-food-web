import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom'
import Signin from '../components/sources/pages/auth/Signin';
import Signup from '../components/sources/pages/auth/Signup';
import Product from '../components/sources/pages/details/Product';
import Cart from '../components/sources/pages/Cart';
import Home from "../components/sources/pages/Home";
import Products from '../components/sources/pages/Products';
import Layout from '../components/sources/utils/Layout';
import axios from 'axios'
import Orders from '../components/sources/pages/Orders';
import Account from '../components/sources/pages/Account';
import Dashboard from '../components/sources/pages/Dashboard';
import DashboardOrders from '../components/sources/pages/dashboard/DashboardOrders';
import DashboardFoods from '../components/sources/pages/dashboard/DashboardFoods';
import DashboardPromo from '../components/sources/pages/dashboard/DashboardPromo';
import DashboardUsers from '../components/sources/pages/dashboard/DashboardUsers';
import AlertTemplate from 'react-alert-template-basic'
import { positions, Provider } from 'react-alert';
import DashboardFranchises from '../components/sources/pages/dashboard/DashboardFranchises';
import DashboardSuppliers from '../components/sources/pages/dashboard/DashboardSuppliers';

const options = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "50px"
}

function launchRender(Component, isLoggedIn, member, adress) {
  return (
    <Layout isLoggedIn={isLoggedIn} member={member}>
      <Component member={member} adress={adress} />
    </Layout>
  )
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: null,
      member: {},
      adress: {},
      rank: undefined,
      loaded: false
    };
  };

  componentDidMount = () => {
    this.loginStatus();
  }

  loginStatus = () => {
    axios.get('/api/auth/logged_in',
      { withCredentials: true })
      .then(response => {
        if (response.data.logged_in) {
          this.handleLogin(response.data)
        } else {
          this.handleLogout()
        }
      })
      .catch(error => console.log('api errors:', error))
  }

  handleLogout = () => {
    this.setState({
      isLoggedIn: false,
      member: {},
      adress: {},
      loaded: true
    })
  }

  handleLogin = (data) => {
    this.setState({
      isLoggedIn: true,
      member: data.member,
      adress: data.adress,
      rank: data.rank,
      loaded: true
    })
  }

  render() {
    const { loaded, isLoggedIn, member, adress } = this.state;
    return (
      <Provider template={AlertTemplate} {...options}>
        <Router>
          {loaded &&
            <Routes>
              {
                isLoggedIn ? (
                  <Fragment>
                    <Route exact path="/orders" element={launchRender(Orders, isLoggedIn, member, adress)} />
                    <Route exact path="/account" element={launchRender(Account, isLoggedIn, member, adress)} />

                    {
                      //administrateur accès
                      member.rank_id == 1 &&
                      <Fragment>
                        <Route exact path="/dashboard/members" element={launchRender(DashboardUsers, isLoggedIn, member, adress)} />
                        <Route exact path="/dashboard/franchises" element={launchRender(DashboardFranchises, isLoggedIn, member, adress)} />
                      </Fragment>
                    }

                    {
                      //Gérant accès
                      member.rank_id <= 2 &&
                      <Fragment>
                        <Route exact path="/dashboard/foods" element={launchRender(DashboardFoods, isLoggedIn, member, adress)} />
                        <Route exact path="/dashboard/code_promos" element={launchRender(DashboardPromo, isLoggedIn, member, adress)} />
                        <Route exact path="/dashboard/suppliers" element={launchRender(DashboardSuppliers, isLoggedIn, member, adress)} />
                      </Fragment>
                    }

                    {
                      //employé accès
                      member.rank_id != 4 &&
                      <Fragment>
                        <Route exact path="/dashboard" element={launchRender(Dashboard, isLoggedIn, member, adress)} />
                        <Route exact path="/dashboard/orders" element={launchRender(DashboardOrders, isLoggedIn, member, adress)} />
                      </Fragment>
                    }
                  </Fragment>
                ) : (
                  <Fragment>
                    <Route exact path="/auth/signup" element={<Signup />} />
                    <Route exact path="/auth/signin" element={<Signin handleLogin={this.handleLogin} />} />
                  </Fragment>)
              }

              <Route exact path="/" element={launchRender(Home, isLoggedIn, member, adress)} />
              <Route exact path="/products" element={launchRender(Products, isLoggedIn, member, adress)} />
              <Route exact path="/details/product/:id" element={launchRender(Product, isLoggedIn, member, adress)} />
              <Route exact path="/cart" element={launchRender(Cart, isLoggedIn, member, adress)} />
              <Route
                path="*"
                element={<Navigate to="/" replace />}
              />
            </Routes>
          }
        </Router>
      </Provider>
    );
  }
};

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(React.createElement(App, null), document.getElementById('app'))
});