class Group < ApplicationRecord
    has_many :franchises, dependent: :destroy
    belongs_to :adress

    validates :name, presence: true
end
