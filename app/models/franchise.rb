class Franchise < ApplicationRecord
    belongs_to :adress
    belongs_to :group

    has_many :ingredient_franchises
    has_many :ingredients, through: :ingredient_franchises
    has_many :suppliers, through: :ingredient_franchises
    
    has_many :suppliers

    validates :name, presence: true
end
