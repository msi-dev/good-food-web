class Review < ApplicationRecord
    belongs_to :food, optional: true
    belongs_to :menu, optional: true
end
