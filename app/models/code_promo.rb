class CodePromo < ApplicationRecord
    belongs_to :type_promo
    belongs_to :franchise, optional: true

    has_many :orders, dependent: :destroy
end
