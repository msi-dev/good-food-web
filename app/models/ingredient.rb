class Ingredient < ApplicationRecord
    has_many :ingredient_franchises
    has_many :franchises, through: :ingredient_franchises
    has_many :suppliers, through: :ingredient_franchises
end
