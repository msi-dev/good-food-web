class PanierCommande < ApplicationRecord
   belongs_to :menu, optional: true
   belongs_to :order, optional: true
   belongs_to :food, optional: true
end
