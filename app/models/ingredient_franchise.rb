class IngredientFranchise < ApplicationRecord
   belongs_to :ingredient
   belongs_to :franchise
   belongs_to :supplier
end
