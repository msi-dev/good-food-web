class Delivery < ApplicationRecord
    belongs_to :adress
    belongs_to :member
    belongs_to :order

    validates :delivery_date, presence: true
end
