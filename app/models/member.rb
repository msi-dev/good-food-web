class Member < ApplicationRecord
    has_secure_password
    validates :email, presence: true
    validates :email, uniqueness: true
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

    belongs_to :rank
    belongs_to :adress, optional: true
    belongs_to :franchise, optional: true

    has_many :orders, dependent: :destroy
    has_many :deliveries, dependent: :destroy
end
