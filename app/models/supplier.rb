class Supplier < ApplicationRecord
    has_many :ingredient_franchises
    has_many :franchises, through: :ingredient_franchises
    has_many :ingredients, through: :ingredient_franchises

    belongs_to :franchise

    validates :name, presence: true
end
