class Menu < ApplicationRecord
    has_one_attached :image
    
    def url
        if object.image.attached?
            Rails.application.routes.url_helpers.rails_blob_url(object.image.blob, only_path: true)
        end
    end

    belongs_to :franchise, optional: true

    has_many :reviews, dependent: :destroy

    has_many :panier_commandes
    has_many :foods, through: :panier_commandes
    has_many :orders, through: :panier_commandes
end
