class Order < ApplicationRecord
    belongs_to :code_promo, optional: true
    belongs_to :franchise
    belongs_to :member
    belongs_to :order_status

    has_many :panier_commandes
    has_many :foods, through: :panier_commandes
    has_many :menus, through: :panier_commandes
end
