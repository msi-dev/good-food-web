class Food < ApplicationRecord
    has_one_attached :image

    def url
        if image.attached?
            Rails.application.routes.url_helpers.rails_blob_url(image.blob, only_path: true)
        end
    end

    belongs_to :category
    belongs_to :franchise, optional: true

    has_many :reviews, dependent: :destroy
end
