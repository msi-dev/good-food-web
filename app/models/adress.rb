class Adress < ApplicationRecord
    has_many :members, dependent: :destroy
    has_many :franchises, dependent: :destroy
    has_many :groups, dependent: :destroy
    has_many :deliveries, dependent: :destroy

    validates :street, presence: true
    validates :street_number, presence: true
    validates :city, presence: true
    validates :postcode, presence: true
    validates :country, presence: true
end
