class TypePromo < ApplicationRecord
    has_many :code_promos, dependent: :destroy

    validates :name, presence: true
end
