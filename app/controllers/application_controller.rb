class ApplicationController < ActionController::Base
    protect_from_forgery with: :null_session
    skip_before_action :verify_authenticity_token

    helper_method :login!, :logged_in?, :current_member, :authorized_member?, :logout!, :set_member, :is_admin?, :is_user?, :is_gerant?, :is_employe?, :is_session_expired?, :reset_session_timer

    # Éxecuté après chaque action
    after_action :reset_session_timer

    def login!
        session[:member_id] = @member.id
        session[:expires_at] = Time.now + 1.hours
    end

    def logged_in?
        !!session[:member_id]
    end

    def current_member
        @current_member ||= Member.find(session[:member_id]) if session[:member_id]
    end

    def authorized_member?
        @member == current_member
    end

    def logout!
        cookies.clear
        reset_session
    end

    def set_member
        @member = Member.find_by(id: session[:member_id])
    end

    def is_admin?
        current_member.rank.name == "Administrateur"
    end

    def is_user?
        current_member.rank.name == "Utilisateur"
    end

    def is_gerant?
        current_member.rank.name == "Gérant"
    end

    def is_employe?
        current_member.rank.name == "Employé"
    end

    def is_session_expired?
        session[:expires_at] < Time.now
    end

    def reset_session_timer
        session[:expires_at] = Time.now + 1.hours
    end

    def verify_login_and_timer
        autorised_path = ['', '/', '/login', '/api/auth/checkLogin', '/api/auth/logged_in', '/api/auth/login', '/auth/signup']

        # Évite les accès à la base de données sans être connecté
        if !logged_in? && !request.fullpath.in?(autorised_path)
            redirect_to "/"
        else
            if !session[:expires_at].blank?
                # Déconnecte l'utilisateur après 1 heure d'inactivité
                if is_session_expired?
                    logout!
                end
            end
        end
    end
end
