class Api::MembersController < ApplicationController

=begin
  @api {get} /api/members/ Get all members
  @apiName index
  @apiGroup Member

  @apiSuccess {json} data List of every member.
=end
  def index
    members = Member.all

    render json: MemberSerializer.new(members).serialized_json
  end

=begin
  @api {get} /api/members/:id Get a member
  @apiName show
  @apiGroup Member

  @apiParam {Number} id Member unique ID.

  @apiSuccess {json} data Data of the corresponding member.
=end
  def show
    member = Member.find_by(id: params[:id])

    render json: MemberSerializer.new(member).serialized_json
  end

=begin
  @api {post} /api/members/ Create a member
  @apiName create
  @apiGroup Member

  @apiParam {json} member Json object about the member.

  @apiSuccess {json} data Data of the created member.
  @apiError {json} error Error description
=end
  def create
    if params[:member][:password].nil?
      params[:member] = params[:member].merge(:password => params[:password])
      #puts params[:member]
    end

    member = Member.new(member_params)

    if member.save
      render json: MemberSerializer.new(member).serialized_json
    else
      #puts member.errors.messages
      render json: { error: member.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/members/ Update a member
  @apiName update
  @apiGroup Member

  @apiParam {json} member Json object about the member.

  @apiSuccess {json} data Data of the updated member.
  @apiError {json} error Error description
=end
  def update
    member = Member.find_by(params[:id])

    if !member_params_update[:adress].nil?
      if member.adress.nil?
        member.adress = Adress.create(member_params_update[:adress])
      else
        member.adress.update(member_params_update[:adress])
      end
    end

    if member.update(member_params_update.except(:adress))
      render json: MemberSerializer.new(member).serialized_json
    else
      render json: { error: member.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/members/:id Delete a member
  @apiName destroy
  @apiGroup Member

  @apiParam {Number} id Member unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    member = Member.find_by(params[:id])

    if member.destroy
      head :no_content
    else
      render json: { error: member.errors.messages }, status: 422
    end
  end

  private
  
  def member_params
    params.require(:member).permit(:lastname, :firstname, :password, :email, :rank_id)
  end

  def member_params_update
    params.require(:member).permit(:lastname, :firstname, :password, :email, :rank_id, :phone, :adress => [:street, :street_number, :city, :postcode, :country])
  end
end
