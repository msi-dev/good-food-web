class Api::IngredientsController < ApplicationController

=begin
  @api {get} /api/ingredients/ Get all ingredients
  @apiName index
  @apiGroup Ingredient

  @apiSuccess {json} data List of every ingredient.
=end
  def index
    ingredients = Ingredient.all

    render json: IngredientSerializer.new(ingredients).serialized_json
  end

=begin
  @api {get} /api/ingredients/:id Get a ingredient
  @apiName show
  @apiGroup Ingredient

  @apiParam {Number} id Ingredient unique ID.

  @apiSuccess {json} data Data of the corresponding ingredient.
=end
  def show
    ingredient = Ingredient.find_by(id: params[:id])

    render json: IngredientSerializer.new(ingredient).serialized_json
  end

=begin
  @api {post} /api/ingredients/ Create a ingredient
  @apiName create
  @apiGroup Ingredient

  @apiParam {json} ingredient Json object about the ingredient.

  @apiSuccess {json} data Data of the created ingredient.
  @apiError {json} error Error description
=end
  def create
    ingredient = Ingredient.new(ingredient_params)

    if ingredient.save
        render json: IngredientSerializer.new(ingredient).serialized_json
    else
        render json: { error: ingredient.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/ingredients/ Update a ingredient
  @apiName update
  @apiGroup Ingredient

  @apiParam {json} ingredient Json object about the ingredient.

  @apiSuccess {json} data Data of the updated ingredient.
  @apiError {json} error Error description
=end
  def update
    ingredient = Ingredient.find_by(id: params[:id])

    if ingredient.update(ingredient_params)
      render json: IngredientSerializer.new(ingredient).serialized_json
    else
      render json: { error: ingredient.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/ingredients/:id Delete a ingredient
  @apiName destroy
  @apiGroup Ingredient

  @apiParam {Number} id Ingredient unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    ingredient = Ingredient.find_by(id: params[:id])

    if ingredient.destroy
        head :no_content
    else
        render json: { error: ingredient.errors.messages }, status: 422
    end
  end

  private

  def ingredient_params
    params.require(:ingredient).permit(:name)
  end
end
