class Api::DeliveriesController < ApplicationController
=begin
  @api {get} /api/deliveries/ Get all deliveries
  @apiName index
  @apiGroup Delivery

  @apiSuccess {json} data List of every delivery.
=end
  def index
    deliveries = Delivery.all

    render json: DeliverySerializer.new(deliveries).serialized_json
  end

=begin
  @api {get} /api/deliveries/:id Get a delivery
  @apiName show
  @apiGroup Delivery

  @apiParam {Number} id Delivery unique ID.

  @apiSuccess {json} data Data of the corresponding delivery.
=end
  def show
    delivery = Delivery.find_by(id: params[:id])

    render json: DeliverySerializer.new(delivery).serialized_json
  end

=begin
  @api {post} /api/deliveries/ Create a delivery
  @apiName create
  @apiGroup Delivery

  @apiParam {json} delivery Json object about the delivery.

  @apiSuccess {json} data Data of the created delivery.
  @apiError {json} error Error description
=end
  def create
    delivery = Delivery.new(delivery_params)

    if delivery.save
        render json: DeliverySerializer.new(delivery).serialized_json
    else
        render json: { error: delivery.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/deliveries/ Update a delivery
  @apiName update
  @apiGroup Delivery

  @apiParam {json} delivery Json object about the delivery.

  @apiSuccess {json} data Data of the updated delivery.
  @apiError {json} error Error description
=end
  def update
    delivery = Delivery.find_by(id: params[:id])

    if delivery.update(delivery_params)
      render json: DeliverySerializer.new(delivery).serialized_json
    else
      render json: { error: delivery.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/deliveries/:id Delete a delivery
  @apiName destroy
  @apiGroup Delivery

  @apiParam {Number} id Delivery unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    delivery = Delivery.find_by(id: params[:id])

    if delivery.destroy
        head :no_content
    else
        render json: { error: delivery.errors.messages }, status: 422
    end
  end

  private

  def delivery_params
    params.require(:delivery).permit(:delivery_date, :adress_id, :member_id)
  end
end
