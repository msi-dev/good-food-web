class Api::CategoriesController < ApplicationController

=begin
  @api {get} /api/categories/ Get all categories
  @apiName index
  @apiGroup Category

  @apiSuccess {json} data List of every category.
=end
  def index
    categories = Category.all

    render json: CategorySerializer.new(categories).serialized_json
  end

=begin
  @api {get} /api/categories/:id Get a category
  @apiName show
  @apiGroup Category

  @apiParam {Number} id Cartegory unique ID.

  @apiSuccess {json} data Data of the corresponding category.
=end
  def show
    category = Category.find_by(id: params[:id])

    render json: CategorySerializer.new(category).serialized_json
  end

=begin
  @api {post} /api/categories/ Create a category
  @apiName create
  @apiGroup Category

  @apiParam {json} category Json object about the category.

  @apiSuccess {json} data Data of the created category.
  @apiError {json} error Error description
=end
  def create
    category = Category.new(category_params)

    if category.save
        render json: CategorySerializer.new(category).serialized_json
    else
        render json: { error: category.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/categories/ Update a category
  @apiName update
  @apiGroup Category

  @apiParam {json} category Json object about the category.

  @apiSuccess {json} data Data of the updated category.
  @apiError {json} error Error description
=end
  def update
    category = Category.find_by(id: params[:id])

    if category.update(category_params)
      render json: CategorySerializer.new(category).serialized_json
    else
      render json: { error: category.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/categories/:id Delete a category
  @apiName destroy
  @apiGroup Category

  @apiParam {Number} id Category unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    category = Category.find_by(id: params[:id])

    if category.destroy
        head :no_content
    else
        render json: { error: category.errors.messages }, status: 422
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
