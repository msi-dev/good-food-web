class Api::GroupsController < ApplicationController

=begin
  @api {get} /api/groups/ Get all groups
  @apiName index
  @apiGroup Group

  @apiSuccess {json} data List of every group.
=end
  def index
    groups = Group.all

    render json: GroupSerializer.new(groups).serialized_json
  end

=begin
  @api {get} /api/groups/:id Get a group
  @apiName show
  @apiGroup Group

  @apiParam {Number} id Group unique ID.

  @apiSuccess {json} data Data of the corresponding group.
=end
  def show
    group = Group.find_by(id: params[:id])

    render json: GroupSerializer.new(group).serialized_json
  end

=begin
  @api {post} /api/groups/ Create a group
  @apiName create
  @apiGroup Group

  @apiParam {json} group Json object about the group.

  @apiSuccess {json} data Data of the created group.
  @apiError {json} error Error description
=end
  def create
    group = Group.new(group_params)

    if group.save
        render json: GroupSerializer.new(group).serialized_json
    else
        render json: { error: group.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/groups/ Update a group
  @apiName update
  @apiGroup Group

  @apiParam {json} group Json object about the group.

  @apiSuccess {json} data Data of the updated group.
  @apiError {json} error Error description
=end
  def update
    group = Group.find_by(id: params[:id])

    if group.update(group_params)
      render json: GroupSerializer.new(group).serialized_json
    else
      render json: { error: group.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/groups/:id Delete a group
  @apiName destroy
  @apiGroup Group

  @apiParam {Number} id Group unique ID.

  @apiSuccess {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    group = Group.find_by(id: params[:id])

    if group.destroy
        head :no_content
    else
        render json: { error: group.errors.messages }, status: 422
    end
  end

  private

  def group_params
    params.require(:group).permit(:name)
  end
end
