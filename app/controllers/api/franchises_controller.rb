class Api::FranchisesController < ApplicationController

=begin
  @api {get} /api/franchises/ Get all franchises
  @apiName index
  @apiGroup Franchise

  @apiSuccess {json} data List of every franchise.
=end
  def index
    franchises = Franchise.all

    render json: franchises, include: [:adress, :group]
  end

=begin
  @api {get} /api/franchises/:id Get a franchise
  @apiName show
  @apiGroup Franchise

  @apiParam {Number} id Franchise unique ID.

  @apiSuccess {json} data Data of the corresponding franchise.
=end
  def show
    franchise = Franchise.find_by(id: params[:id])

    render json: franchise, include: [:adress, :group]
  end

=begin
  @api {post} /api/franchises/ Create a franchise
  @apiName create
  @apiGroup Franchise

  @apiParam {json} franchise Json object about the franchise.

  @apiSuccess {json} data Data of the created franchise.
  @apiError {json} error Error description
=end
  def create
    if params[:franchise][:adress].nil?
      params[:franchise] = params[:franchise].merge(:adress => params[:adress])
    end

    adresse = Adress.find_or_create_by(street: franchise_params[:adress][:street], 
                                       street_number: franchise_params[:adress][:street_number],
                                       city: franchise_params[:adress][:city],
                                       postcode: franchise_params[:adress][:postcode],
                                       country: franchise_params[:adress][:country])
                                       
    franchise = Franchise.new(franchise_params.except(:adress))
    franchise.adress_id = adresse.id



    if franchise.save
        render json: franchise, include: [:adress, :group]
    else
        render json: { error: franchise.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/franchises/ Update a franchise
  @apiName update
  @apiGroup Franchise

  @apiParam {json} franchise Json object about the franchise.

  @apiSuccess {json} data Data of the updated franchise.
  @apiError {json} error Error description
=end
  def update
    if params[:franchise][:adress].nil?
      params[:franchise] = params[:franchise].merge(:adress => params[:adress])
    end

    franchise = Franchise.find_by(id: params[:id])

    adresse = franchise.adress
    adresse.update(street: franchise_params[:adress][:street], 
                  street_number: franchise_params[:adress][:street_number],
                  city: franchise_params[:adress][:city],
                  postcode: franchise_params[:adress][:postcode],
                  country: franchise_params[:adress][:country])

    if franchise.update(franchise_params.except(:adress))
      render json: franchise #, include: [:adress, :group]
    else
      render json: { error: franchise.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/franchises/:id Delete a franchise
  @apiName destroy
  @apiGroup Franchise

  @apiParam {Number} id Franchise unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    franchise = Franchise.find_by(id: params[:id])

    if franchise.destroy
        head :no_content
    else
        render json: { error: franchise.errors.messages }, status: 422
    end
  end

  private

  def franchise_params
    params.require(:franchise).permit(:name, :group_id, :adress => [:street, :street_number, :city, :postcode, :country])
  end
end
