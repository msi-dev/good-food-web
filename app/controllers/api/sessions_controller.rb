class Api::SessionsController < ApplicationController

=begin
  @api {post} /auth/login Create a new user session by looking member email
  @apiName create
  @apiGroup Session

  @apiParam {json} member Json object about the member.

  @apiSuccess {json} data Data of the member.
  @apiError {json} error No member found
=end
    def create
        @member = Member.find_by_email(session_params[:email])

        if @member && @member.authenticate(session_params[:password])
            login!
            render json: {
                logged_in: true,
                member: @member,
                rank: @member.rank.name,
                adress: @member.adress
                #verified: @member.user_verified
            }
        else
            render json: { error: 'Aucun membre trouvé' }, status: 422
        end
    end

=begin
  @api {post} /auth/checklogin Create a new user session by looking member email
  @apiName checkLogin
  @apiGroup Session

  @apiParam {json} member Json object about the member.

  @apiSuccess {json} data Data of the member.
  @apiError {json} error No member found
=end
    def checkLogin
        @member = Member.find_by_email(session_params[:email])
        if !@member.nil?
            if @member && @member.authenticate(session_params[:password])
              render json: {
                  logged_in: true,
                  member: @member,
                  rank: @member.rank.name,
                  adress: @member.adress
              }
            else
              render json: { error: 'Mot de passe erroné' }, status: 422
            end
        else
          render json: { error: 'Adresse mail incorrect' }, status: 422
        end
    end

=begin
  @api {get} /auth/logged_in Verify is user is logged
  @apiName is_logged_in?
  @apiGroup Session

  @apiSuccess {json} data Data of the member.
  @apiError {json} error No member found
=end
    def is_logged_in?
        if logged_in? && current_member
          render json: {
            logged_in: true,
            member: current_member,
            rank: current_member.rank.name,
            adress: current_member.adress
          }
        else
          render json: {
            logged_in: false,
            message: 'Aucun membre'
          }
        end
    end

=begin
  @api {post} /auth/login Disconnect user
  @apiName destroy
  @apiGroup Session

  @apiSuccess {json} data Data confirming logout.
=end
    def destroy
        logout!
        render json: {
          status: 200,
          logged_out: true
        }
    end

    private

    def session_params
        params.require(:member).permit(:email, :password)
    end

end
