class Api::CodePromosController < ApplicationController

=begin
  @api {get} /api/code_promos/ Get all code promo
  @apiName index
  @apiGroup CodePromo

  @apiSuccess {json} data List of every code promo.
=end
  def index
    code_promos = CodePromo.all

    render json: CodePromoSerializer.new(code_promos).serialized_json
  end

=begin
  @api {get} /api/code_promos/:id Get a code promo
  @apiName show
  @apiGroup CodePromo

  @apiParam {Number} id CodePromo unique ID.

  @apiSuccess {json} data Data of the corresponding code promo.
=end
  def show
    code_promo = CodePromo.find_by(id: params[:id])

    render json: CodePromoSerializer.new(code_promo).serialized_json
  end

=begin
  @api {post} /api/code_promos/ Create a code promo
  @apiName create
  @apiGroup CodePromo

  @apiParam {json} code_promo Json object about the code promo.

  @apiSuccess {json} data Data of the created code promo.
  @apiError {json} error Error description
=end
  def create
    code_promo = CodePromo.new(code_promo_params)

    if code_promo.save
        render json: CodePromoSerializer.new(code_promo).serialized_json
    else
        render json: { error: code_promo.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/code_promos/ Update a code promo
  @apiName update
  @apiGroup CodePromo

  @apiParam {json} code_promo Json object about the code promo.

  @apiSuccess {json} data Data of the updated code promo.
  @apiError {json} error Error description
=end
  def update
    code_promo = CodePromo.find_by(id: params[:id])

    if code_promo.update(code_promo_params)
      render json: CodePromoSerializer.new(code_promo).serialized_json
    else
      render json: { error: code_promo.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/code_promos/:id Delete a code promo
  @apiName destroy
  @apiGroup CodePromo

  @apiParam {Number} id CodePromo unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    code_promo = CodePromo.find_by(id: params[:id])

    if code_promo.destroy
        head :no_content
    else
        render json: { error: code_promo.errors.messages }, status: 422
    end
  end

=begin
  @api {post} /api/code_promos/apply/:code_name Get the code promo discount value
  @apiName apply_code
  @apiGroup CodePromo

  @apiParam {Number} code_name Code giving a discount.

  @apiSuccess (200) {json} a json array containing the discount value and the code promo id.
  @apiError {json} error The code is expired or doesn't exists.
=end
  def apply_code
    code_promo = CodePromo.find_by(code_name: params[:code_name])

    if !code_promo.nil?
        total_product = code_promo_apply_params[:total_amount]
        remise = 0

        if code_promo.type_promo.name == "Réduction"
          remise = code_promo.amount

          # On ne va quand même pas donner de l'argent aux clients
          if (total_product - remise) < 0
            remise = total_product
          end

        else
          remise = total_product * (code_promo.amount / 100)
        end

        render json: { remise: remise, code_promo_id: code_promo.id }
    else
        render json: { error: "Ce code promo n'existe pas ou est expiré." }, status: 422
    end
  end

  private

  def code_promo_params
    params.require(:code_promo).permit(:code_name, :amount, :franchise_id, :type_promo_id)
  end

  def code_promo_apply_params
    params.permit(:total_amount)
  end
end
