class Api::IngredientFranchisesController < ApplicationController

=begin
  @api {get} /api/ingredient_franchises/ Get all ingredient franchises
  @apiName index
  @apiGroup IngredientFranchise

  @apiSuccess {json} data List of every ingredient franchise.
=end
  def index
    ingredient_franchises = IngredientFranchise.all

    render json: IngredientFranchiseSerializer.new(ingredient_franchises).serialized_json
  end

=begin
  @api {get} /api/ingredient_franchises/:id Get a ingredient franchise
  @apiName show
  @apiGroup IngredientFranchise

  @apiParam {Number} id IngredientFranchise unique ID.

  @apiSuccess {json} data Data of the corresponding ingredient franchise.
=end
  def show
    ingredient_franchise = IngredientFranchise.find_by(id: params[:id])

    render json: IngredientFranchiseSerializer.new(ingredient_franchise).serialized_json
  end

=begin
  @api {post} /api/ingredient_franchises/ Create a ingredient franchise
  @apiName create
  @apiGroup IngredientFranchise

  @apiParam {json} ingredient_franchise Json object about the ingredient franchise.

  @apiSuccess {json} data Data of the created ingredient franchise.
  @apiError {json} error Error description
=end
  def create
    ingredient_franchise = IngredientFranchise.new(ingredient_franchise_params)

    if ingredient_franchise.save
        render json: IngredientFranchiseSerializer.new(ingredient_franchise).serialized_json
    else
        render json: { error: ingredient_franchise.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/ingredient_franchises/ Update a ingredient franchise
  @apiName update
  @apiGroup IngredientFranchise

  @apiParam {json} ingredient_franchise Json object about the ingredient franchise.

  @apiSuccess {json} data Data of the updated ingredient_franchise.
  @apiError {json} error Error description
=end
  def update
    ingredient_franchise = IngredientFranchise.find_by(id: params[:id])

    if ingredient_franchise.update(ingredient_franchise_params)
      render json: IngredientFranchiseSerializer.new(ingredient_franchise).serialized_json
    else
      render json: { error: ingredient_franchise.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/ingredient_franchises/:id Delete a ingredient franchise
  @apiName destroy
  @apiGroup IngredientFranchise

  @apiParam {Number} id IngredientFranchise unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    ingredient_franchise = IngredientFranchise.find_by(id: params[:id])

    if ingredient_franchise.destroy
        head :no_content
    else
        render json: { error: ingredient_franchise.errors.messages }, status: 422
    end
  end

  private

  def ingredient_franchise_params
    params.require(:ingredient_franchise).permit(:quantity, :ingredient_id, :franchise_id, :supplier_id)
  end
end
