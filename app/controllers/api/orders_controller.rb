class Api::OrdersController < ApplicationController

=begin
  @api {get} /api/orders/ Get orders of connected customer or all of them for employees. Ordered by status.
  @apiName index
  @apiGroup Order

  @apiSuccess {json} data List of every order.
=end
  def index
    orders = []

    if current_member.rank.name == "Client"
      orders = Order.all.where(member_id: current_member.id).order(:order_status_id)
    else
      orders = Order.all.order(:order_status_id)
    end

    render json: orders, include: [:order_status, :foods]
  end

=begin
  @api {get} /api/orders/status:id Get all orders by order status
  @apiName index_by_status
  @apiGroup Order

  @apiParam {Number} id Status unique ID.

  @apiSuccess {json} data List of every order.
=end
  def index_by_status
    orders = Order.joins(:order_status).all.
          where(:order_status_id => params[:id])

    render json: orders, include: [:order_status, :foods]
  end

=begin
  @api {get} /api/orders/:id Get an order
  @apiName show
  @apiGroup Order

  @apiParam {Number} id Order unique ID.

  @apiSuccess {json} data Data of the corresponding order.
=end
  def show
    order = Order.find_by(id: params[:id])

    render json: OrderSerializer.new(order).serialized_json
  end

=begin
  @api {post} /api/orders/ Create an order
  @apiName create
  @apiGroup Order

  @apiParam {json} order Json object about the order.

  @apiSuccess {json} data Data of the created order.
  @apiError {json} error Error description
=end
  def create
    order = Order.new(order_create_params)

    # Données par défaut des infos pas encore gérés
    order.order_date = DateTime.now
    order.order_status_id = 1
    order.franchise_id = 1

    if order.code_promo_id == 0
      order.code_promo_id = nil
    end

    if order.member_id.nil? || order.member_id = 0
      order.member_id = current_member.id
    end

    if order.save
      # Enregistrement des nourritures
      params[:order][:foods].each {|f| 
        PanierCommande.create([order_id: order.id, food_id: f[:id], menu_id: nil])
      }

        render json: OrderSerializer.new(order).serialized_json
    else
        render json: { error: order.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/orders/ Update an order
  @apiName update
  @apiGroup Order

  @apiParam {json} order Json object about the order.

  @apiSuccess {json} data Data of the updated order.
  @apiError {json} error Error description
=end
  def update
    order = Order.find_by(id: params[:id])

    if order.update(order_params)
      render json: OrderSerializer.new(order).serialized_json
    else
      render json: { error: order.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/orders/:id Delete an order
  @apiName destroy
  @apiGroup Food

  @apiParam {Number} id Food unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    order = Order.find_by(id: params[:id])

    if order.destroy
        head :no_content
    else
        render json: { error: order.errors.messages }, status: 422
    end
  end

  private

  def order_params
    params.require(:order).permit(:order_date, :name, :total_amount, :franchise_id, :code_promo_id, :member_id, :order_status_id)
  end

  def order_create_params
    params.require(:order).permit(:total_amount, :code_promo_id, :member_id)
  end
end
