class Api::PreparationsController < ApplicationController

=begin
  @api {get} /api/preparations/ Get all preparations
  @apiName index
  @apiGroup Preparation

  @apiSuccess {json} data List of every preparation.
=end
  def index
    preparations = Preparation.all

    render json: PreparationSerializer.new(preparations).serialized_json
  end

=begin
  @api {get} /api/preparations/:id Get a preparation
  @apiName show
  @apiGroup Preparation

  @apiParam {Number} id Preparation unique ID.

  @apiSuccess {json} data Data of the corresponding preparation.
=end
  def show
    preparation = Preparation.find_by(id: params[:id])

    render json: PreparationSerializer.new(preparation).serialized_json
  end

=begin
  @api {post} /api/preparations/ Create a preparation
  @apiName create
  @apiGroup Preparation

  @apiParam {json} preparation Json object about the preparation.

  @apiSuccess {json} data Data of the created preparation.
  @apiError {json} error Error description
=end
  def create
    preparation = Preparation.new(preparation_params)

    if preparation.save
        render json: PreparationSerializer.new(preparation).serialized_json
    else
        render json: { error: preparation.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/preparations/ Update a preparation
  @apiName update
  @apiGroup Preparation

  @apiParam {json} preparation Json object about the preparation.

  @apiSuccess {json} data Data of the updated preparation.
  @apiError {json} error Error description
=end
  def update
    preparation = Preparation.find_by(id: params[:id])

    if preparation.update(preparation_params)
      render json: PreparationSerializer.new(preparation).serialized_json
    else
      render json: { error: preparation.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/preparations/:id Delete a preparation
  @apiName destroy
  @apiGroup Preparation

  @apiParam {Number} id Preparation unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    preparation = Preparation.find_by(id: params[:id])

    if preparation.destroy
        head :no_content
    else
        render json: { error: preparation.errors.messages }, status: 422
    end
  end

  private

  def preparation_params
    params.require(:preparation).permit(:progress, :order_id)
  end
end
