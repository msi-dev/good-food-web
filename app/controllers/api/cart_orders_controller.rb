class Api::CartOrdersController < ApplicationController

=begin
  @api {get} /api/cart_orders/ Get all cart order
  @apiName index
  @apiGroup CartOrder

  @apiSuccess {json} data List of every cart order.
=end
  def index
    cart_orders = PanierCommande.all

    render json: PanierCommandeSerializer.new(cart_orders).serialized_json
  end

=begin
  @api {get} /api/cart_orders/:id Get a cart order
  @apiName show
  @apiGroup CartOrder

  @apiParam {Number} id Cart order unique ID.

  @apiSuccess {json} data Data of the corresponding cart order.
=end
  def show
    cart_order = PanierCommande.find_by(id: params[:id])

    render json: PanierCommandeSerializer.new(cart_order).serialized_json
  end

=begin
  @api {post} /api/cart_orders/ Create a cart order
  @apiName create
  @apiGroup CartOrder

  @apiParam {json} cart_order Json object about the cart order.

  @apiSuccess {json} data Data of the created cart order.
  @apiError {json} error Error description
=end
  def create
    cart_order = PanierCommande.new(cart_order_params)

    if cart_order.save
        render json: PanierCommandeSerializer.new(cart_order).serialized_json
    else
        render json: { error: cart_order.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/cart_orders/ Update a cart order
  @apiName update
  @apiGroup CartOrder

  @apiParam {json} cart_order Json object about the cart order.

  @apiSuccess {json} data Data of the updated cart order.
  @apiError {json} error Error description
=end
  def update
    cart_order = PanierCommande.find_by(id: params[:id])

    if cart_order.update(cart_order_params)
      render json: PanierCommandeSerializer.new(cart_order).serialized_json
    else
      render json: { error: cart_order.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/cart_orders/:id Delete a cart order
  @apiName destroy
  @apiGroup CartOrder

  @apiParam {Number} id CartOrder unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    cart_order = PanierCommande.find_by(id: params[:id])

    if cart_order.destroy
        head :no_content
    else
        render json: { error: cart_order.errors.messages }, status: 422
    end
  end

  private

  def cart_order_params
    params.require(:panier_commande).permit(:quantity, :ingredient_id, :franchise_id, :supplier_id)
  end
end
