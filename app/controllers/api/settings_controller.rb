class Api::SettingsController < ApplicationController

=begin
  @api {get} /api/settings/ Get all settings
  @apiName index
  @apiGroup Setting

  @apiSuccess {json} data List of every setting.
=end
  def index
    settings = Setting.all

    render json: SettingSerializer.new(settings).serialized_json
  end

=begin
  @api {post} /api/settings/ Create a setting
  @apiName create
  @apiGroup Setting

  @apiParam {json} setting Json object about the setting.

  @apiSuccess {json} data Data of the created setting.
  @apiError {json} error Error description
=end
  def create
    setting = Setting.new(setting_params)

    if setting.save
        render json: SettingSerializer.new(setting).serialized_json
    else
        render json: { error: setting.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/settings/ Update a setting
  @apiName update
  @apiGroup Setting

  @apiParam {json} setting Json object about the setting.

  @apiSuccess {json} data Data of the updated setting.
  @apiError {json} error Error description
=end
  def update
    setting = Setting.find_by(id: params[:id])

    if setting.update(setting_params)
      render json: SettingSerializer.new(setting).serialized_json
    else
      render json: { error: setting.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/settings/:id Delete a setting
  @apiName update
  @apiGroup Setting

  @apiParam {Number} id Setting unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    setting = Setting.find_by(id: params[:id])

    if setting.destroy
        head :no_content
    else
        render json: { error: setting.errors.messages }, status: 422
    end
  end

  private

  def setting_param
    params.require(:setting).permit(:mailSmtpAdress, :mailSmtpPort, :mailAdress, :encrypted_psw, :encrypted_psw_iv, :pswd)
  end
end
