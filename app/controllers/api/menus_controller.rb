class Api::MenusController < ApplicationController

=begin
  @api {get} /api/menus/ Get all menus
  @apiName index
  @apiGroup Menu

  @apiSuccess {json} data List of every menu.
=end
  def index
    menus = Menu.all

    render json: MenuSerializer.new(menus).serialized_json
  end

=begin
  @api {get} /api/menus/:id Get a menu
  @apiName show
  @apiGroup Menu

  @apiParam {Number} id Menu unique ID.

  @apiSuccess {json} data Data of the corresponding menu.
=end
  def show
    menu = Menu.find_by(id: params[:id])

    render json: MenuSerializer.new(menu).serialized_json
  end

=begin
  @api {post} /api/menus/ Create a menu
  @apiName create
  @apiGroup Menu

  @apiParam {json} menu Json object about the menu.

  @apiSuccess {json} data Data of the created menu.
  @apiError {json} error Error description
=end
  def create
    menu = Menu.new(menu_params)

    if menu.save
        render json: MenuSerializer.new(menu).serialized_json
    else
        render json: { error: menu.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/menus/ Update a menu
  @apiName update
  @apiGroup Menu

  @apiParam {json} menu Json object about the menu.

  @apiSuccess {json} data Data of the updated menu.
  @apiError {json} error Error description
=end
  def update
    menu = Menu.find_by(id: params[:id])

    if menu.update(menu_params)
      render json: MenuSerializer.new(menu).serialized_json
    else
      render json: { error: menu.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/menus/:id Delete a menu
  @apiName destroy
  @apiGroup Menu

  @apiParam {Number} id Menu unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    menu = Menu.find_by(id: params[:id])

    if menu.destroy
        head :no_content
    else
        render json: { error: menu.errors.messages }, status: 422
    end
  end

  private

  def menu_params
    params.require(:menu).permit(:name, :description, :price, :franchise_id, :code_promo_id, :image)
  end
end
