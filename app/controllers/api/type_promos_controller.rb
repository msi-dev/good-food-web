class Api::TypePromosController < ApplicationController

=begin
  @api {get} /api/type_promos/ Get all types promo
  @apiName index
  @apiGroup TypePromo

  @apiSuccess {json} data List of every type promo.
=end
  def index
    type_promos = TypePromo.all

    render json: TypePromoSerializer.new(type_promos).serialized_json
  end

=begin
  @api {get} /api/type_promos/:id Get a type promo
  @apiName show
  @apiGroup TypePromo

  @apiParam {Number} id TypePromo unique ID.

  @apiSuccess {json} data Data of the corresponding type promo.
=end
  def show
    type_promo = TypePromo.find_by(id: params[:id])

    render json: TypePromoSerializer.new(type_promo).serialized_json
  end

=begin
  @api {post} /api/type_promos/ Create a type promo
  @apiName create
  @apiGroup TypePromo

  @apiParam {json} type_promo Json object about the type promo.

  @apiSuccess {json} data Data of the created type promo.
  @apiError {json} error Error description
=end
  def create
    type_promo = TypePromo.new(type_promo_params)

    if type_promo.save
        render json: TypePromoSerializer.new(type_promo).serialized_json
    else
        render json: { error: type_promo.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/type_promos/ Update a type promo
  @apiName update
  @apiGroup TypePromo

  @apiParam {json} type_promo Json object about the type promo.

  @apiSuccess {json} data Data of the updated type promo.
  @apiError {json} error Error description
=end
  def update
    type_promo = TypePromo.find_by(id: params[:id])

    if type_promo.update(type_promo_params)
      render json: TypePromoSerializer.new(type_promo).serialized_json
    else
      render json: { error: type_promo.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/type_promos/:id Delete a type promo
  @apiName destroy
  @apiGroup TypePromo

  @apiParam {Number} id TypePromo unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    type_promo = TypePromo.find_by(id: params[:id])

    if type_promo.destroy
        head :no_content
    else
        render json: { error: type_promor.errors.messages }, status: 422
    end
  end

  private

  def type_promo_params
    params.require(:type_promo).permit(:name)
  end
end
