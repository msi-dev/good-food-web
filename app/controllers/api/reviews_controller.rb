class Api::ReviewsController < ApplicationController

=begin
  @api {get} /api/reviews/ Get all reviews
  @apiName index
  @apiGroup Review

  @apiSuccess {json} data List of every review.
=end
  def index
    reviews = Review.all

    render json: ReviewSerializer.new(reviews).serialized_json
  end

=begin
  @api {post} /api/reviews/ Create a review
  @apiName create
  @apiGroup Review

  @apiParam {json} review Json object about the review.

  @apiSuccess {json} data Data of the created review.
  @apiError {json} error Error description
=end
  def create
    review = Review.new(review_params)

    if review.save
        render json: ReviewSerializer.new(review).serialized_json
    else
        render json: { error: review.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/reviews/ Update a review
  @apiName update
  @apiGroup Review

  @apiParam {json} review Json object about the review.

  @apiSuccess {json} data Data of the updated review.
  @apiError {json} error Error description
=end
  def update
    review = Review.find_by(id: params[:id])

    if review.update(review_params)
      render json: ReviewSerializer.new(review).serialized_json
    else
      render json: { error: review.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/ranks/:id Delete a review
  @apiName destroy
  @apiGroup Review

  @apiParam {Number} id Review unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    review = Review.find_by(id: params[:id])

    if review.destroy
        head :no_content
    else
        render json: { error: review.errors.messages }, status: 422
    end
  end

  private

  def review_param
    params.require(:review).permit(:note, :comment, :menu_id, :review_id)
  end
end
