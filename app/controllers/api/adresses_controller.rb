class Api::AdressesController < ApplicationController

=begin
  @api {get} /api/adresses/ Get all adresses
  @apiName index
  @apiGroup Adress

  @apiSuccess {json} data List of every adress.
=end
  def index
    adresses = Adress.all

    render json: AdressSerializer.new(adresses).serialized_json
  end

=begin
  @api {get} /api/adresses/:id Get an adress
  @apiName show
  @apiGroup Adress

  @apiParam {Number} id Adress unique ID.

  @apiSuccess {json} data Data of the corresponding adress.
=end
  def show
    adress = Adress.find_by(id: params[:id])

    render json: AdressSerializer.new(adress).serialized_json
  end

=begin
  @api {post} /api/adresses/ Create an adress
  @apiName create
  @apiGroup Adress

  @apiParam {json} adress Json object about the adress.

  @apiSuccess {json} data Data of the created adress.
  @apiError {json} error Error description
=end
  def create
    adress = Adress.new(adress_params)

    if adress.save
        render json: AdressSerializer.new(adress).serialized_json
    else
        render json: { error: adress.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/adresses/ Update an adress
  @apiName update
  @apiGroup Adress

  @apiParam {json} adress Json object about the adress.

  @apiSuccess {json} data Data of the updated adress.
  @apiError {json} error Error description
=end
  def update
    adress = Adress.find_by(id: params[:id])

    if adress.update(adress_params)
        render json: AdressSerializer.new(adress).serialized_json
    else
        render json: { error: adress.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/adresses/:id Delete an adress
  @apiName destroy
  @apiGroup Adress

  @apiParam {Number} id Adress unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    adress = Adress.find_by(id: params[:id])

    if adress.destroy
        render :no_content
    else
        render json: { error: adress.errors.messages }, status: 422
    end
  end

  private

  def adress_params
    params.require(:adress).permit(:street, :street_number, :city, :postcode, :country)
  end
end
