class Api::RanksController < ApplicationController
  
=begin
  @api {get} /api/ranks/ Get all ranks
  @apiName index
  @apiGroup Rank

  @apiSuccess {json} data List of every rank.
=end
  def index
    ranks = Rank.all

    render json: RankSerializer.new(ranks).serialized_json
  end

=begin
  @api {get} /api/ranks/:id Get a rank
  @apiName show
  @apiGroup Rank

  @apiParam {Number} id Rank unique ID.

  @apiSuccess {json} data Data of the corresponding rank.
=end
  def show
    rank = Rank.find_by(id: params[:id])

    render json: RankSerializer.new(rank).serialized_json
  end

=begin
  @api {post} /api/ranks/ Create a rank
  @apiName create
  @apiGroup Rank

  @apiParam {json} rank Json object about the rank.

  @apiSuccess {json} data Data of the created rank.
  @apiError {json} error Error description
=end
  def create
    rank = Rank.new(rank_params)

    if rank.save
        render json: RankSerializer.new(rank).serialized_json
    else
        render json: { error: rank.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/ranks/ Update a rank
  @apiName update
  @apiGroup Rank

  @apiParam {json} rank Json object about the rank.

  @apiSuccess {json} data Data of the updated rank.
  @apiError {json} error Error description
=end
  def update
    rank = Rank.find_by(id: params[:id])

    if rank.update(rank_params)
      render json: RankSerializer.new(rank).serialized_json
    else
      render json: { error: rank.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/ranks/:id Delete a rank
  @apiName destroy
  @apiGroup Rank

  @apiParam {Number} id Rank unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    rank = Rank.find_by(id: params[:id])

    if rank.destroy
        head :no_content
    else
        render json: { error: rank.errors.messages }, status: 422
    end
  end

  private

  def rank_params
    params.require(:rank).permit(:name)
  end
end
