class Api::SuppliersController < ApplicationController

=begin
  @api {get} /api/suppliers/ Get all suppliers
  @apiName index
  @apiGroup Supplier

  @apiSuccess {json} data List of every supplier.
=end
  def index
    suppliers = Supplier.all

    render json: SupplierSerializer.new(suppliers).serialized_json
  end

=begin
  @api {get} /api/suppliers/:id Get a supplier
  @apiName show
  @apiGroup Supplier

  @apiParam {Number} id Supplier unique ID.

  @apiSuccess {json} data Data of the corresponding supplier.
=end
  def show
    supplier = Supplier.find_by(id: params[:id])

    render json: SupplierSerializer.new(supplier).serialized_json
  end

=begin
  @api {post} /api/suppliers/ Create a supplier
  @apiName create
  @apiGroup Supplier

  @apiParam {json} supplier Json object about the supplier.

  @apiSuccess {json} data Data of the created supplier.
  @apiError {json} error Error description
=end
  def create
    supplier = Supplier.new(supplier_params)

    if supplier.save
        render json: SupplierSerializer.new(supplier).serialized_json
    else
        render json: { error: supplier.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/suppliers/ Update a supplier
  @apiName update
  @apiGroup Supplier

  @apiParam {json} supplier Json object about the supplier.

  @apiSuccess {json} data Data of the updated supplier.
  @apiError {json} error Error description
=end
  def update
    supplier = Supplier.find_by(id: params[:id])

    if supplier.update(supplier_params)
      render json: SupplierSerializer.new(supplier).serialized_json
    else
      render json: { error: supplier.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/suppliers/:id Delete a supplier
  @apiName destroy
  @apiGroup Supplier

  @apiParam {Number} id Supplier unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    supplier = Supplier.find_by(id: params[:id])

    if supplier.destroy
        head :no_content
    else
        render json: { error: supplier.errors.messages }, status: 422
    end
  end

  private

  def supplier_params
    params.require(:supplier).permit(:name, :franchises_id)
  end
end
