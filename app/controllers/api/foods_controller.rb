class Api::FoodsController < ApplicationController

=begin
  @api {get} /api/foods/ Get all foods
  @apiName index
  @apiGroup Food

  @apiSuccess {json} data List of every food.
=end
  def index
    foods = Food.all

    render json: FoodSerializer.new(foods).serialized_json
  end

=begin
  @api {get} /api/foods/category/:id Get all foods by category
  @apiName index_by_category
  @apiGroup Food

  @apiParam {Number} id Category unique ID.

  @apiSuccess {json} data List of every food related to the category.
=end
  def index_by_category
    foods = Food.all.where(category_id: params[:id])

    render json: foods, include: [:category], :methods => :url
  end

=begin
  @api {get} /api/foods/:id Get a food
  @apiName show
  @apiGroup Food

  @apiParam {Number} id Food unique ID.

  @apiSuccess {json} data Data of the corresponding food.
=end
  def show
    food = Food.find_by(id: params[:id])

    render json: food, include: [:category], :methods => :url
  end

=begin
  @api {post} /api/foods/ Create a food
  @apiName create
  @apiGroup Food

  @apiParam {json} food Json object about the food.

  @apiSuccess {json} data Data of the created food.
  @apiError {json} error Error description
=end
  def create
    food = Food.new(food_params)
    print params
    if food.save
        render json: FoodSerializer.new(food).serialized_json
    else
        render json: { error: food.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/foods/ Update a food
  @apiName update
  @apiGroup Food

  @apiParam {json} food Json object about the food.

  @apiSuccess {json} data Data of the updated food.
  @apiError {json} error Error description
=end
  def update
    food = Food.find_by(id: params[:id])
    # Création d'un nouveau produit actif, l'ancien est désactivé
    if food.update(food_params)
      render json: FoodSerializer.new(food).serialized_json
    else
      render json: { error: food.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/foods/:id Delete a food
  @apiName destroy
  @apiGroup Food

  @apiParam {Number} id Food unique ID.

  @apiSuccess {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    food = Food.find_by(id: params[:id])
    if food.destroy
        head :no_content
    else
        render json: { error: food.errors.messages }, status: 422
    end
  end

  private

  def food_params
    params.permit(:name, :description, :number_consumer, :price, :category_id, :franchise_id, :is_active, :is_archived, :image)
  end
end
