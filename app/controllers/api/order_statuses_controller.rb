class Api::OrderStatusesController < ApplicationController

=begin
  @api {get} /api/order_statuses/ Get all order statuses
  @apiName index
  @apiGroup OrderStatus

  @apiSuccess {json} data List of every order status.
=end
  def index
    order_statuses = OrderStatus.all

    render json: order_statuses, include: [:orders]
  end

=begin
  @api {get} /api/order_statuses/:id Get a order status
  @apiName show
  @apiGroup OrderStatus

  @apiParam {Number} id OrderStatus unique ID.

  @apiSuccess {json} data Data of the corresponding order status.
=end
  def show
    order_status = OrderStatus.find_by(id: params[:id])

    render json: order_status, include: [:orders]
  end

=begin
  @api {post} /api/order_statuses/ Create a order status
  @apiName create
  @apiGroup OrderStatus

  @apiParam {json} order status Json object about the order status.

  @apiSuccess {json} data Data of the created order status.
  @apiError {json} error Error description
=end
  def create
    order_status = OrderStatus.new(order_status_params)

    if order_status.save
        render json: OrderStatusSerializer.new(order_status).serialized_json
    else
        render json: { error: order_status.errors.messages }, status: 422
    end
  end

=begin
  @api {put} /api/order_statuses/ Update a order status
  @apiName update
  @apiGroup OrderStatus

  @apiParam {json} order status Json object about the order status.

  @apiSuccess {json} data Data of the updated order_status.
  @apiError {json} error Error description
=end
  def update
    order_status = OrderStatus.find_by(id: params[:id])

    if order_status.update(order_status_params)
      render json: OrderStatusSerializer.new(order_status).serialized_json
    else
      render json: { error: order_status.errors.messages }, status: 422
    end
  end

=begin
  @api {delete} /api/order_statuses/:id Delete a order status
  @apiName destroy
  @apiGroup OrderStatus

  @apiParam {Number} id OrderStatus unique ID.

  @apiSuccess  {String} nothing 204 returns nothing.
  @apiError {json} error Error description
=end
  def destroy
    order_status = OrderStatus.find_by(id: params[:id])

    if order_status.destroy
        head :no_content
    else
        render json: { error: order_status.errors.messages }, status: 422
    end
  end

  private

  def order_status_params
    params.require(:order_status).permit(:name, :description)
  end
end
