class OrderStatusSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :description

  has_many :orders
end
