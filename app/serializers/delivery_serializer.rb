class DeliverySerializer
  include FastJsonapi::ObjectSerializer
  attributes :delivery_date, :adress_id, :member_id, :order_id
end
