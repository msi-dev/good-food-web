class PanierCommandeSerializer
  include FastJsonapi::ObjectSerializer
  attributes :menu_id, :food_id, :order_id
end
