class FoodSerializer
  include FastJsonapi::ObjectSerializer
  
  attributes :name, :description, :number_consumer, :price, :category_id, :is_active, :is_archived, :url

  has_one :category
end
