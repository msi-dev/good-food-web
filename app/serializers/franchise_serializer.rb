class FranchiseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :global_rate, :open_date
end
