class AdressSerializer
  include FastJsonapi::ObjectSerializer
  attributes :street, :street_number, :city, :postcode, :country

  has_many :members
  has_many :franchises
  has_many :groups
end
