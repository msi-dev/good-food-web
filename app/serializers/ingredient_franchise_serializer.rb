class IngredientFranchiseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :ingredient_id, :franchise_id, :supplier_id, :quantity
end
