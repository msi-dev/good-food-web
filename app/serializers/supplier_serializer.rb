class SupplierSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :franchises_id
end
