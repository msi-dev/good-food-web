class SettingSerializer
  include FastJsonapi::ObjectSerializer
  attributes :mailSmtpAdress, :mailSmtpPort, :mailAdress, :encrypted_psw, :encrypted_psw_iv
end
