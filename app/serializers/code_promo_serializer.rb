class CodePromoSerializer
  include FastJsonapi::ObjectSerializer
  attributes :code_name, :amount, :type_promo_id, :franchise_id
end
