class GroupSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :adress_id

  has_many :franchises
end
