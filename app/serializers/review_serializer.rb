class ReviewSerializer
  include FastJsonapi::ObjectSerializer
  attributes :note, :comment, :menu_id, :food_id
end
