class OrderSerializer
  include FastJsonapi::ObjectSerializer
  attributes :order_date, :name, :total_amount, :code_promo_id, :franchise_id, :member_id
end
