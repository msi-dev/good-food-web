class MenuSerializer
  include FastJsonapi::ObjectSerializer

  attributes :name, :description, :price, :code_promo_id, :franchise_id, :url

end
