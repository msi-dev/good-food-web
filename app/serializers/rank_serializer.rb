class RankSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name

  has_many :members
end
