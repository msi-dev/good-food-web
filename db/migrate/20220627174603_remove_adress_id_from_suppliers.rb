class RemoveAdressIdFromSuppliers < ActiveRecord::Migration[6.1]
  def change
    remove_column :suppliers, :adress_id, :bigint

    add_reference :suppliers, :franchises, foreign_key: true
  end
end
