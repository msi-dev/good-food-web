class CreateGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :groups do |t|
      t.string :name, limit: 50, null: false
      t.belongs_to :adress, null: false, foreign_key: true

      t.timestamps
    end
  end
end
