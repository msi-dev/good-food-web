class CreateMenus < ActiveRecord::Migration[6.1]
  def change
    create_table :menus do |t|
      t.string :name, limit: 30, null: false
      t.text :description
      t.decimal :price, null: false, default: 0, precision: 5, scale: 2
      t.belongs_to :code_promo, null: true, foreign_key: true
      t.belongs_to :franchise, null: true, foreign_key: true

      t.timestamps
    end
  end
end
