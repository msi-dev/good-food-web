class RemoveNotNullToAdressIdInMember < ActiveRecord::Migration[6.1]
  def change
    change_column_null :members, :adress_id, true
  end
end
