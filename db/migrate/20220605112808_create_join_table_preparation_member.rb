class CreateJoinTablePreparationMember < ActiveRecord::Migration[6.1]
  def change
    create_join_table :preparations, :members do |t|
      #t.references :preparation, null: false, foreign_key: true
      #t.references :member, null: false, foreign_key: true

      t.timestamps
    end

    add_foreign_key(:members_preparations, :preparations)
    add_foreign_key(:members_preparations, :members)
  end
end
