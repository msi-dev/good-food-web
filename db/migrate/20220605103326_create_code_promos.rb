class CreateCodePromos < ActiveRecord::Migration[6.1]
  def change
    create_table :code_promos do |t|
      t.string :code_name, limit: 30, null: false
      t.decimal :amount, null: false, default: 0, precision: 5, scale: 2
      t.belongs_to :type_promo, null: false, foreign_key: true
      t.belongs_to :franchise, null: true, foreign_key: true

      t.timestamps
    end
  end
end
