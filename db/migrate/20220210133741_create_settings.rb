class CreateSettings < ActiveRecord::Migration[6.1]
  def change
    create_table :settings do |t|
      t.string :mailSmtpAdress, limit: 50
      t.integer :mailSmtpPort
      t.string :mailAdress, limit: 50
      t.string :encrypted_psw, limit: 255
      t.string :encrypted_psw_iv, limit: 18

      t.timestamps
    end
  end
end
