class DropPreparations < ActiveRecord::Migration[6.1]
  def change
    drop_table :preparations
  end
end
