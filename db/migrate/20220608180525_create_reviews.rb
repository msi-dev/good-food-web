class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.decimal :note, null: false, precision: 3, scale: 2
      t.check_constraint "note >= 1 AND note <= 5" 
      t.text :comment
      t.belongs_to :food, null: true, foreign_key: true
      t.belongs_to :menu, null: true, foreign_key: true

      t.timestamps
    end
  end
end
