class CreateAdresses < ActiveRecord::Migration[6.1]
  def change
    create_table :adresses do |t|
      t.string :street, limit: 50, null: false
      t.integer :street_number, null: false
      t.string :city, limit: 50, null: false
      t.string :postcode, limit: 5, null: false
      t.string :country, limit: 50, null: false

      t.timestamps
    end
  end
end
