class DropMembersPreparations < ActiveRecord::Migration[6.1]
  def change
    drop_join_table :members, :preparations 
  end
end
