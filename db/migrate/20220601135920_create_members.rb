class CreateMembers < ActiveRecord::Migration[6.1]
  def change
    create_table :members do |t|
      t.string :firstname, limit: 50, null: false
      t.string :lastname, limit: 50, null: false
      t.string :email, limit: 50, null: false
      t.string :password, limit: 50, null: false
      t.string :phone, limit: 20
      t.boolean :activate_2FA, default: false, null: false
      t.datetime :last_connection
      t.string :password_digest, limit: 64, null: false
      t.string :token, limit: 64
      t.boolean :user_verified, default: false, null: false
      t.belongs_to :rank, null: false, foreign_key: true
      t.belongs_to :adress, null: false, foreign_key: true
      t.belongs_to :franchise, null: true, foreign_key: true

      t.timestamps
    end
  end
end
