class CreatePreparations < ActiveRecord::Migration[6.1]
  def change
    create_table :preparations do |t|
      t.decimal :progress, null: false, default: 0, precision: 5, scale: 2
      t.belongs_to :order, null: false, foreign_key: true

      t.timestamps
    end
  end
end
