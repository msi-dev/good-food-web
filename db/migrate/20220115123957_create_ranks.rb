class CreateRanks < ActiveRecord::Migration[6.1]
  def change
    create_table :ranks do |t|
      t.string :name, limit: 50, null: false

      t.timestamps
    end
  end
end
