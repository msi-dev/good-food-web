class ChangeFranchisesIdToFranchiseId < ActiveRecord::Migration[6.1]
  def change
    rename_column :suppliers, :franchises_id, :franchise_id
  end
end
