class CreateOrderStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :order_statuses do |t|
      t.string :name, limit: 30, null: false
      t.text :description

      t.timestamps
    end

    add_reference :orders, :order_status, foreign_key: true
  end
end
