class CreateFranchises < ActiveRecord::Migration[6.1]
  def change
    create_table :franchises do |t|
      t.string :name, limit: 50, null: false
      t.decimal :global_rate, precision: 5, scale: 2
      t.datetime :open_date
      t.belongs_to :adress, null: false, foreign_key: true
      t.belongs_to :group, null: false, foreign_key: true

      t.timestamps
    end
  end
end
