class CreateIngredientFranchises < ActiveRecord::Migration[6.1]
  def change
    create_table :ingredient_franchises do |t|
      t.references :ingredient, null: true, foreign_key: true
      t.references :franchise, null: true, foreign_key: true
      t.references :supplier, null: true, foreign_key: true
      t.integer :quantity, default: 1, null: false

      t.timestamps
    end
  end
end
