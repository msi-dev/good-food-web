class RemoveCodePromoIdFromMenus < ActiveRecord::Migration[6.1]
  def change
    remove_column :menus, :code_promo_id, :bigint
  end
end
