class CreatePanierCommandes < ActiveRecord::Migration[6.1]
  def change
    create_table :panier_commandes do |t|
      t.references :menu, null: true, foreign_key: true
      t.references :food, null: true, foreign_key: true
      t.references :order, null: true, foreign_key: true

      t.timestamps
    end
  end
end
