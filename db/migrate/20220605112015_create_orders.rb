class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.datetime :order_date, null: false
      t.string :name, limit: 100
      t.decimal :total_amount, null: false, default: 0, precision: 5, scale: 2
      t.belongs_to :code_promo, null: true, foreign_key: true
      t.belongs_to :franchise, null: false, foreign_key: true
      t.belongs_to :member, null: false, foreign_key: true

      t.timestamps
    end
  end
end
