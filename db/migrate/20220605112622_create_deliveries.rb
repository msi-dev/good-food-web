class CreateDeliveries < ActiveRecord::Migration[6.1]
  def change
    create_table :deliveries do |t|
      t.datetime :delivery_date, null: false
      t.belongs_to :adress, null: false, foreign_key: true
      t.belongs_to :member, null: true, foreign_key: true
      t.belongs_to :order, null: false, foreign_key: true

      t.timestamps
    end
  end
end
