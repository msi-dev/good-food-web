class CreateFoods < ActiveRecord::Migration[6.1]
  def change
    create_table :foods do |t|
      t.string :name, limit: 30, null: false
      t.text :description
      t.integer :number_consumer, null: false, default: 1
      t.decimal :price, null: false, default: 0, precision: 5, scale: 2
      t.belongs_to :category, null: false, foreign_key: true
      t.belongs_to :franchise, null: true, foreign_key: true
      t.boolean :is_active, default: true, null: false
      t.boolean :is_archived, default: false, null: false

      t.timestamps
    end
  end
end
