# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_28_170649) do

  create_table "active_storage_attachments", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "adresses", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "street", limit: 50, null: false
    t.integer "street_number", null: false
    t.string "city", limit: 50, null: false
    t.string "postcode", limit: 5, null: false
    t.string "country", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "categories", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 30, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "code_promos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "code_name", limit: 30, null: false
    t.decimal "amount", precision: 5, scale: 2, default: "0.0", null: false
    t.bigint "type_promo_id", null: false
    t.bigint "franchise_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["franchise_id"], name: "index_code_promos_on_franchise_id"
    t.index ["type_promo_id"], name: "index_code_promos_on_type_promo_id"
  end

  create_table "deliveries", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "delivery_date", null: false
    t.bigint "adress_id", null: false
    t.bigint "member_id"
    t.bigint "order_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["adress_id"], name: "index_deliveries_on_adress_id"
    t.index ["member_id"], name: "index_deliveries_on_member_id"
    t.index ["order_id"], name: "index_deliveries_on_order_id"
  end

  create_table "foods", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 30, null: false
    t.text "description"
    t.integer "number_consumer", default: 1, null: false
    t.decimal "price", precision: 5, scale: 2, default: "0.0", null: false
    t.bigint "category_id", null: false
    t.bigint "franchise_id"
    t.boolean "is_active", default: true, null: false
    t.boolean "is_archived", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_foods_on_category_id"
    t.index ["franchise_id"], name: "index_foods_on_franchise_id"
  end

  create_table "franchises", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.decimal "global_rate", precision: 5, scale: 2
    t.datetime "open_date"
    t.bigint "adress_id", null: false
    t.bigint "group_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["adress_id"], name: "index_franchises_on_adress_id"
    t.index ["group_id"], name: "index_franchises_on_group_id"
  end

  create_table "groups", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.bigint "adress_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["adress_id"], name: "index_groups_on_adress_id"
  end

  create_table "ingredient_franchises", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "ingredient_id"
    t.bigint "franchise_id"
    t.bigint "supplier_id"
    t.integer "quantity", default: 1, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["franchise_id"], name: "index_ingredient_franchises_on_franchise_id"
    t.index ["ingredient_id"], name: "index_ingredient_franchises_on_ingredient_id"
    t.index ["supplier_id"], name: "index_ingredient_franchises_on_supplier_id"
  end

  create_table "ingredients", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "members", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "firstname", limit: 50, null: false
    t.string "lastname", limit: 50, null: false
    t.string "email", limit: 50, null: false
    t.string "phone", limit: 20
    t.boolean "activate_2FA", default: false, null: false
    t.datetime "last_connection"
    t.string "password_digest", limit: 64, null: false
    t.string "token", limit: 64
    t.boolean "user_verified", default: false, null: false
    t.bigint "rank_id", null: false
    t.bigint "adress_id"
    t.bigint "franchise_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["adress_id"], name: "index_members_on_adress_id"
    t.index ["franchise_id"], name: "index_members_on_franchise_id"
    t.index ["rank_id"], name: "index_members_on_rank_id"
  end

  create_table "menus", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 30, null: false
    t.text "description"
    t.decimal "price", precision: 5, scale: 2, default: "0.0", null: false
    t.bigint "franchise_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["franchise_id"], name: "index_menus_on_franchise_id"
  end

  create_table "order_statuses", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 30, null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "orders", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "order_date", null: false
    t.string "name", limit: 100
    t.decimal "total_amount", precision: 5, scale: 2, default: "0.0", null: false
    t.bigint "code_promo_id"
    t.bigint "franchise_id", null: false
    t.bigint "member_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "order_status_id"
    t.index ["code_promo_id"], name: "index_orders_on_code_promo_id"
    t.index ["franchise_id"], name: "index_orders_on_franchise_id"
    t.index ["member_id"], name: "index_orders_on_member_id"
    t.index ["order_status_id"], name: "index_orders_on_order_status_id"
  end

  create_table "panier_commandes", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "menu_id"
    t.bigint "food_id"
    t.bigint "order_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["food_id"], name: "index_panier_commandes_on_food_id"
    t.index ["menu_id"], name: "index_panier_commandes_on_menu_id"
    t.index ["order_id"], name: "index_panier_commandes_on_order_id"
  end

  create_table "ranks", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reviews", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.decimal "note", precision: 3, scale: 2, null: false
    t.text "comment"
    t.bigint "food_id"
    t.bigint "menu_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["food_id"], name: "index_reviews_on_food_id"
    t.index ["menu_id"], name: "index_reviews_on_menu_id"
    t.check_constraint "(`note` >= 1) and (`note` <= 5)"
  end

  create_table "settings", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "mailSmtpAdress", limit: 50
    t.integer "mailSmtpPort"
    t.string "mailAdress", limit: 50
    t.string "encrypted_psw"
    t.string "encrypted_psw_iv", limit: 18
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "suppliers", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "franchise_id"
    t.index ["franchise_id"], name: "index_suppliers_on_franchise_id"
  end

  create_table "type_promos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "code_promos", "franchises"
  add_foreign_key "code_promos", "type_promos"
  add_foreign_key "deliveries", "adresses"
  add_foreign_key "deliveries", "members"
  add_foreign_key "deliveries", "orders"
  add_foreign_key "foods", "categories"
  add_foreign_key "foods", "franchises"
  add_foreign_key "franchises", "adresses"
  add_foreign_key "franchises", "groups"
  add_foreign_key "groups", "adresses"
  add_foreign_key "ingredient_franchises", "franchises"
  add_foreign_key "ingredient_franchises", "ingredients"
  add_foreign_key "ingredient_franchises", "suppliers"
  add_foreign_key "members", "adresses"
  add_foreign_key "members", "franchises"
  add_foreign_key "members", "ranks"
  add_foreign_key "menus", "franchises"
  add_foreign_key "orders", "code_promos"
  add_foreign_key "orders", "franchises"
  add_foreign_key "orders", "members"
  add_foreign_key "orders", "order_statuses"
  add_foreign_key "panier_commandes", "foods"
  add_foreign_key "panier_commandes", "menus"
  add_foreign_key "panier_commandes", "orders"
  add_foreign_key "reviews", "foods"
  add_foreign_key "reviews", "menus"
  add_foreign_key "suppliers", "franchises"
end
