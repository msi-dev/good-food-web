# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

orders_statuses = OrderStatus.create([
    {
        name: "En préparation",
        description: "Nos cuisiniers mettent tout en oeuvre pour réaliser votre commande."
    },
    {
        name: "Livraison en cours",
        description: "Notre livreur est en chemin vers votre adresse."
    },
    {
        name: "Commande livrée",
        description: "N'hésitez pas à laisser un avis sur les plats de votre commande."
    }
])

ranks = Rank.create([
    {
        name: "Administrateur"
    },
    {
        name: "Gérant"
    },
    {
        name: "Employé"
    },
    {
        name: "Client"
    }
])

groups = Group.create([
    {
        name: "Groupe de Sarthe",
        adress: Adress.create(city: "Le Mans", country: "France", postcode: 72100, street: "Rue du test", street_number: 1)
    },
    {
        name: "Groupe de Mayenne",
        adress: Adress.create(city: "Laval", country: "France", postcode: 12345, street: "Rue du test n°2", street_number: 43)
    },
    {
        name: "Groupe d'Ile-de-France",
        adress: Adress.create(city: "Paris", country: "France", postcode: 54321, street: "Rue du test n°3", street_number: 17)
    }
])

franchises = Franchise.create([
    {
        name: "Franchise de Sablé Sur Sarthe",
        adress: Adress.create(city: "Sablé Sur Sarthe", country: "France", postcode: 14725, street: "Rue du test n°4", street_number: 120),
        group: Group.first,
        global_rate: 2.36,
        open_date: DateTime.new(2020, 8, 3, 15, 0, 0)
    },
    {
        name: "Franchise de Paris",
        adress: Adress.find_by_city("Paris"),
        group: Group.find_by_name("Groupe d'Ile-de-France"),
        global_rate: 5,
        open_date: DateTime.new(2019, 5, 16, 10, 0, 0)
    },
    {
        name: "Franchise de Neuilly",
        adress: Adress.find_by_city("Paris"),
        group: Group.find_by_name("Groupe d'Ile-de-France"),
        global_rate: 4.2,
        open_date: DateTime.new(2019, 11, 22, 8, 0, 0)
    }
])

type_promo = TypePromo.create([
    {
        name: "Réduction"
    },
    {
        name: "Pourcentage"
    }
])

code_promo = CodePromo.create([
    {
        code_name: "PROMO_TEST_PCT",
        amount: 25,
        type_promo: TypePromo.last
    },
    {
        code_name: "PROMO_TEST_MONTANT",
        amount: 17.99,
        type_promo: TypePromo.first,
        franchise: Franchise.first
    },
])

categories = Category.create([
    {
        name: "Burgers"
    },
    {
        name: "Sodas"
    },
    {
        name: "Boissons chaudes"
    },
    {
        name: "Pizzas"
    },
    {
        name: "Autres"
    }
])

foods = Food.create([
    {
        name: "Big burger",
        description: "Le plus gros burger que vous avez jamais mangé !",
        price: 16.22,
        number_consumer: 3,
        category: Category.first,
        franchise: Franchise.find_by_name("Franchise de Paris")
    },
    {
        name: "Mac Burger",
        description: "Un burger avec une sensation de plagiat.",
        price: 8.22,
        category: Category.first
    },
    {
        name: "Pepsi",
        description: "Pepsi.",
        price: 2.22,
        category: Category.find_by_name("Sodas")
    },
    {
        name: "Coca",
        description: "Coca.",
        price: 3.22,
        category: Category.find_by_name("Sodas")
    },
    {
        name: "Café",
        description: "Café.",
        price: 5.22,
        category: Category.find_by_name("Boissons chaudes")
    },
    {
        name: "Thé",
        description: "Thé.",
        price: 6.22,
        category: Category.find_by_name("Boissons chaudes")
    },
    {
        name: "Provencale",
        description: "Une délicieuse pizza, exclu Sablé Sur Sarthe !",
        price: 13.22,
        number_consumer: 2,
        franchise: Franchise.first,
        category: Category.find_by_name("Pizzas")
    },
    {
        name: "Margarita",
        description: "Miam pizza",
        price: 8.62,
        category: Category.find_by_name("Pizzas")
    },
    {
        name: "Lazagne au cheval",
        description: "C'est bon. Il paraît.",
        price: 9.32,
        number_consumer: 4,
        category: Category.find_by_name("Autres")
    },
    {
        name: "Gâteau au chocolat",
        description: "Tout le monde en mange. Alors achête !",
        price: 9.32,
        category: Category.find_by_name("Autres")
    }
])

menus = Menu.create([
    {
        name: "Menu Plaisir",
        description: "Un bon menu pour se mettre bien.",
        price: 15.92
    },
    {
        name: "Menu luxe",
        description: "Exclu Neuilly ! Pour les riches.",
        price: 420.69,
        franchise: Franchise.last
    }
])

