FactoryBot.define do
  factory :category do
    sequence(:name) { |i| "Catégorie n° #{i}" } 
  end
end
