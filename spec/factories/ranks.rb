FactoryBot.define do
  factory :rank, aliases: [:rang] do
    sequence(:name) { |i| "Rang n° #{i}" } 
  end
end
