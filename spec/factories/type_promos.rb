FactoryBot.define do
  factory :type_promo do
    sequence(:name) { |i| "Type Promo n° #{i}" } 
  end
end
