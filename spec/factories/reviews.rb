FactoryBot.define do
  factory :review do
    note { 3 }
    comment { "C'est bien" }
  end
end
