FactoryBot.define do
  factory :supplier do
    sequence(:name) { |i| "Fournisseur n° #{i}" } 
  end
end
