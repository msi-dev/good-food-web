FactoryBot.define do
  factory :order_status do
    name { "Status de test" }
    description { "C'est le status de test" }
  end
end
