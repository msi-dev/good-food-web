FactoryBot.define do
  factory :setting do
    mailSmtpAdress { 'smtp.gmail.com' } 
    mailSmtpPort { 587 }
    mailAdress { 'azerty@uiop.fr' }
    encrypted_pswd { SecureRandom.alphanumeric(255) }
    encrypted_pswd_iv { SecureRandom.alphanumeric(18) }
  end
end
