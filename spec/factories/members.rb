FactoryBot.define do
  factory :member, aliases: [:user] do
    rank
    firstname {'Test'}
    lastname {'Membre'} 
    email { "#{SecureRandom.alphanumeric(15)}@truc.org"} 
    password {'azeaze'} 
    user_verified {false}
  
    factory :member_2, aliases: [:user2] do
      rank
      firstname {'Membre'}
      lastname {'NuméroDeux'} 
      email {"#{SecureRandom.alphanumeric(15)}@test.fr"} 
      password {'azerergfvdwx'} 
      user_verified {true}
    end
  end

end
