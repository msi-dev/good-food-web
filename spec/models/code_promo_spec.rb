require 'rails_helper'

RSpec.describe CodePromo, type: :model do
  context 'CRUD' do
    before do 
      @code_promo = CodePromo.create(
                                code_name: "PROMO_TEST",
                                amount: 25,
                                type_promo: TypePromo.create(name: "Test")
                              )
    end

    it 'should persist a code promo' do
      expect{ CodePromo.create(
                  code_name: "PROMO_TEST_2",
                  amount: 13.44,
                  type_promo: TypePromo.create(name: "Test n°2")
                ) }.to change{ CodePromo.count }.by(1)
    end 

    it 'should update a code promo' do
      @code_promo = CodePromo.first
      @code_promo.update(code_name: 'NEW_CODE')
  
      expect(@code_promo.code_name).to eq('NEW_CODE')
    end

    it 'should delete a code promo' do
      code_promo = CodePromo.last

      expect{ code_promo.destroy }.to change{ CodePromo.count }.by(-1)
    end

    it 'should read codes promo' do
      codes_promo = CodePromo.all

      expect(CodePromo.count).to eq(codes_promo.count)
    end
  end
end
