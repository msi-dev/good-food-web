require 'rails_helper'

RSpec.describe Adress, type: :model do
  context 'CRUD' do
    before do 
      @adress = Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120)
    end

    it 'should persist a adress' do
      expect{ Adress.create(city: "Test 7",
                            country: "Test",
                            postcode: 16554,
                            street: "Rue du test 2",
                            street_number: 120) }.to change{ Adress.count }.by(1)
    end 

    it 'should update a adress' do
      @adress = Adress.first
      @adress.update(city: 'Le Mans')
  
      expect(@adress.city).to eq('Le Mans')
    end

    it 'should delete a adress' do
      adress = Adress.last

      expect{ adress.destroy }.to change{ Adress.count }.by(-1)
    end

    it 'should read adresses' do
      adresses = Adress.all

      expect(Adress.count).to eq(adresses.count)
    end
  end
end
