require 'rails_helper'

RSpec.describe Franchise, type: :model do
  context 'CRUD' do
    before do 
      @franchise = Franchise.create(name: "Franchise de Neuilly",
                                    adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                    group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                    global_rate: 4.2,
                                    open_date: DateTime.new(2019, 11, 22, 8, 0, 0))
    end

    it 'should persist a franchise' do
      expect{ Franchise.create(name: "Franchise de Neuilly",
                                adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                global_rate: 2.2,
                                open_date: DateTime.new(2020, 3, 14, 13, 0, 0)) }
                        .to change{ Franchise.count }.by(1)
    end 

    it 'should update a franchise' do
      @franchise = Franchise.first
      @franchise.update(name: 'New name')
  
      expect(@franchise.name).to eq('New name')
    end

    it 'should delete a franchise' do
      franchise = Franchise.last

      expect{ franchise.destroy }.to change{ Franchise.count }.by(-1)
    end

    it 'should read franchises' do
      franchises = Franchise.all

      expect(Franchise.count).to eq(franchises.count)
    end
  end
end
