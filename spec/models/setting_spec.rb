require 'rails_helper'

RSpec.describe Setting, type: :model do
  context 'CRUD' do
    before do 
      @setting = FactoryBot.create(:setting)
    end

    it 'should persist a setting' do
      expect{ FactoryBot.create(:setting) }.to change{ Setting.count }.by(1)
    end 

    it 'should update a setting' do
      @setting.update(mailSmtpPort: 514)
  
      expect(@setting.mailSmtpPort).to eq(514)
    end

    it 'should delete a setting' do
      setting = Setting.last

      expect{ setting.destroy }.to change{ Setting.count }.by(-1)
    end

    it 'should read settings' do
      settings = Setting.all

      expect(Setting.count).to eq(settings.count)
    end
  end
end
