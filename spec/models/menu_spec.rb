require 'rails_helper'

RSpec.describe Menu, type: :model do
  context 'CRUD' do
    before do 
      @menu = Menu.create(
                    name: "Test",
                    description: "Ceci est un test",
                    price: 18.74
                )
    end

    it 'should persist a menu' do
      expect{ Menu.create(
                      name: "Test 2",
                      description: "Ceci est un test n°2",
                      price: 26.43
                  ) }.to change{ Menu.count }.by(1)
    end 

    it 'should update a menu' do
      @menu = Menu.first
      @menu.update(name: 'New name')
  
      expect(@menu.name).to eq('New name')
    end

    it 'should delete a menu' do
      menu = Menu.last

      expect{ menu.destroy }.to change{ Menu.count }.by(-1)
    end

    it 'should read menus' do
      menus = Menu.all

      expect(Menu.count).to eq(menus.count)
    end
  end
end
