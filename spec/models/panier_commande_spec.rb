require 'rails_helper'

RSpec.describe PanierCommande, type: :model do
  context 'CRUD' do
    before do 
      @panier_commande = PanierCommande.create(food: Food.create(name: "Test",
                                                                  description: "Ceci est un test",
                                                                  price: 16.22,
                                                                  number_consumer: 3,
                                                                  category: Category.create(name: "test")),
                                               order: Order.create(order_date: DateTime.new(2022, 3, 3, 10, 0, 0),
                                                                    name: "Commande de test",
                                                                    total_amount: 25.2,
                                                                    franchise: Franchise.create(name: "Franchise de Test",
                                                                                                adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                                                                group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                                                                global_rate: 4.2,
                                                                                                open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                                                                    member: FactoryBot.create(:member),
                                                                    order_status: OrderStatus.create(name: "Status test")
                                                                  )
                                                )
    end

    it 'should persist a panier commande' do
      expect{ PanierCommande.create(food: Food.create(name: "Test",
                                                      description: "Ceci est un test",
                                                      price: 16.22,
                                                      number_consumer: 3,
                                                      category: Category.create(name: "test")),
                                    order: Order.create(order_date: DateTime.new(2022, 3, 3, 10, 0, 0),
                                            name: "Commande de test",
                                            total_amount: 25.2,
                                            franchise: Franchise.create(name: "Franchise de Test",
                                                                        adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                                        group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                                        global_rate: 4.2,
                                                                        open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                                            member: FactoryBot.create(:member),
                                            order_status: OrderStatus.create(name: "Status test")
                                          )
                                    ) }
                        .to change{ PanierCommande.count }.by(1)
    end 

    it 'should update a panier commande' do
      @panier_commande = PanierCommande.first
      food = @panier_commande.food
      food.update(name: 'New name')
      @panier_commande = PanierCommande.first
  
      expect(@panier_commande.food.name).to eq('New name')
    end

    it 'should delete a panier commande' do
      paniers_commande = PanierCommande.last

      expect{ paniers_commande.destroy }.to change{ PanierCommande.count }.by(-1)
    end

    it 'should read panier commande' do
      paniers_commande = PanierCommande.all

      expect(PanierCommande.count).to eq(paniers_commande.count)
    end
  end
end
