require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'CRUD' do
    before do 
      @group = Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120))
    end

    it 'should persist a group' do
      expect{ Group.create(name: 'Test', adress: Adress.create(city: "machin", country: "France", postcode: 12344, street: "Rue du test 2", street_number: 14)) }.to change{ Group.count }.by(1)
    end 

    it 'should update a group' do
      @group = Group.first
      @group.update(name: 'New name')
  
      expect(@group.name).to eq('New name')
    end

    it 'should delete a group' do
      group = Group.last

      expect{ group.destroy }.to change{ Group.count }.by(-1)
    end

    it 'should read groups' do
      groups = Group.all

      expect(Group.count).to eq(groups.count)
    end
  end
end
