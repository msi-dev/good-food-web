require 'rails_helper'

RSpec.describe Review, type: :model do
  context 'CRUD' do
    before do 
      @review = Review.create(note: 1.2, comment: 'test', menu: Menu.create(name: "Test",
                                                                            description: "Ceci est un test",
                                                                            price: 18.74
                ))
    end

    it 'should persist a review' do
      expect{ Review.create(note: 1.2, comment: 'test', menu: Menu.create(name: "Test",
                                                                            description: "Ceci est un test",
                                                                            price: 18.74
                            )) }
                      .to change{ Review.count }.by(1)
    end 

    it 'should update a review' do
      @review = Review.first
      @review.update(comment: 'New comment')
  
      expect(@review.comment).to eq('New comment')
    end

    it 'should delete a review' do
      review = Review.last

      expect{ review.destroy }.to change{ Review.count }.by(-1)
    end

    it 'should read reviews' do
      reviews = Review.all

      expect(Review.count).to eq(reviews.count)
    end
  end
end
