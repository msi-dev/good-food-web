require 'rails_helper'

RSpec.describe OrderStatus, type: :model do
  context 'CRUD' do
    before do 
      @order_status = OrderStatus.create(name: 'test', description: "C'est une description de test")
    end

    it 'should persist a rank' do
      expect{ OrderStatus.create(name: 'test', description: "C'est une description de test") }
        .to change{ OrderStatus.count }.by(1)
    end 

    it 'should update a rank' do
      @order_status = OrderStatus.first
      @order_status.update(name: 'New name')
  
      expect(@order_status.name).to eq('New name')
    end

    it 'should delete a rank' do
      rank = OrderStatus.last

      expect{ rank.destroy }.to change{ OrderStatus.count }.by(-1)
    end

    it 'should read orders statuses' do
      orders_statuses = OrderStatus.all

      expect(OrderStatus.count).to eq(orders_statuses.count)
    end
  end
end
