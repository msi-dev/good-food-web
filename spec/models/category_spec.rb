require 'rails_helper'

RSpec.describe Category, type: :model do
  context 'CRUD' do
    before do 
      @category = Category.create(name: 'test')
    end

    it 'should persist a category' do
      expect{ Category.create(name: 'Test') }.to change{ Category.count }.by(1)
    end 

    it 'should update a category' do
      @category = Category.first
      @category.update(name: 'New name')
  
      expect(@category.name).to eq('New name')
    end

    it 'should delete a category' do
      category = Category.last

      expect{ category.destroy }.to change{ Category.count }.by(-1)
    end

    it 'should read categorys' do
      categorys = Category.all

      expect(Category.count).to eq(categorys.count)
    end
  end

end
