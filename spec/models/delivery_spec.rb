require 'rails_helper'
require 'date'

RSpec.describe Delivery, type: :model do
  context 'CRUD' do
    before do 
      @delivery = Delivery.create(
                    delivery_date: DateTime.new(2022, 2, 2, 14, 30, 0),
                    adress: Adress.create(city: "Test", country: "Testistan", postcode: 14725, street: "Rue du test", street_number: 120),
                    member: FactoryBot.create(:member),
                    order: Order.create(order_date: DateTime.new(2022, 3, 3, 10, 0, 0),
                                        name: "Commande de test",
                                        total_amount: 25.2,
                                        franchise: Franchise.create(name: "Franchise de Test",
                                                                    adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                                    group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                                    global_rate: 4.2,
                                                                    open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                                        member: FactoryBot.create(:member),
                                        order_status: OrderStatus.create(name: "Status test")
                                      )
                  )
    end

    it 'should persist a delivery' do
      expect{ Delivery.create(
                              delivery_date: DateTime.new(2024, 5, 22, 17, 48, 17),
                              adress: Adress.create(city: "Test n°3", country: "Testistan", postcode: 14725, street: "Rue du test", street_number: 120),
                              member: FactoryBot.create(:member),
                              order: Order.create(order_date: DateTime.new(2023, 12, 25, 22, 22, 22),
                                                  name: "Commande de test n°2",
                                                  total_amount: 25.2,
                                                  franchise: Franchise.create(name: "Franchise de Test",
                                                                              adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                                              group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                                              global_rate: 4.2,
                                                                              open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                                                  member: FactoryBot.create(:member),
                                                  order_status: OrderStatus.create(name: "Status test n°2")
                                                )
                            ) }
                        .to change{ Delivery.count }.by(1)
    end 

    it 'should update a delivery' do
      @delivery = Delivery.first
      @delivery.update(delivery_date: DateTime.new(2023, 5, 5, 9, 0, 0))
  
      expect(@delivery.delivery_date).to eq(DateTime.new(2023, 5, 5, 9, 0, 0))
    end

    it 'should delete a delivery' do
      delivery = Delivery.last

      expect{ delivery.destroy }.to change{ Delivery.count }.by(-1)
    end

    it 'should read deliveries' do
      deliveries = Delivery.all

      expect(Delivery.count).to eq(deliveries.count)
    end
  end
end
