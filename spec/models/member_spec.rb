require 'rails_helper'

RSpec.describe Member, type: :model do
  context 'CRUD' do
    before do 
      @member = FactoryBot.create(:member)
    end

    it 'should persist a member' do
      expect{ FactoryBot.create(:member) }.to change{ Member.count }.by(1)
    end 

    it 'should update a member' do
      @member.update(firstname: 'name')
  
      expect(@member.firstname).to eq('name')
    end

    it 'should delete a member' do
      member = Member.last

      expect{ member.destroy }.to change{ Member.count }.by(-1)
    end

    it 'should read members' do
      members = Member.all

      expect(Member.count).to eq(members.count)
    end
  end

end
