require 'rails_helper'

RSpec.describe Order, type: :model do
  context 'CRUD' do
    before do 
      @order = Order.create(order_date: DateTime.new(2022, 3, 3, 10, 0, 0),
                              name: "Commande de test",
                              total_amount: 25.2,
                              franchise: Franchise.create(name: "Franchise de Test",
                                                          adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                          group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                          global_rate: 4.2,
                                                          open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                              member: FactoryBot.create(:member),
                              order_status: OrderStatus.create(name: "Status test")
                            )
    end

    it 'should persist a order' do
      expect{ Order.create(order_date: DateTime.new(2022, 3, 3, 10, 0, 0),
                            name: "Commande de test n°2",
                            total_amount: 25.2,
                            franchise: Franchise.create(name: "Franchise de Test n°2",
                                                        adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Rue du test", street_number: 120),
                                                        group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "France", postcode: 12345, street: "Rue du test 3", street_number: 10)),
                                                        global_rate: 4.2,
                                                        open_date: DateTime.new(2019, 11, 22, 8, 0, 0)),
                            member: FactoryBot.create(:member),
                            order_status: OrderStatus.create(name: "Status test n°2")
                          ) }
                    .to change{ Order.count }.by(1)
    end 

    it 'should update a order' do
      @order = Order.first
      @order.update(name: 'New name')
  
      expect(@order.name).to eq('New name')
    end

    it 'should delete a order' do
      order = Order.last

      expect{ order.destroy }.to change{ Order.count }.by(-1)
    end

    it 'should read orders' do
      orders = Order.all

      expect(Order.count).to eq(orders.count)
    end
  end
end
