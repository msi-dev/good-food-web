require 'rails_helper'

RSpec.describe Supplier, type: :model do
  context 'CRUD' do
    before do 
      @supplier = Supplier.create(name: 'test',
                                  franchise: Franchise.create(name: "La franchise de test",
                                                                  adress: Adress.create(city: "Test", country: "France", postcode: 14725, street: "Boulevard du test", street_number: 120),
                                                                  group: Group.create(name: 'test', adress: Adress.create(city: "Test", country: "Testistant", postcode: 36985, street: "Rue du test 17", street_number: 185)),
                                                                  global_rate: 1.2,
                                                                  open_date: DateTime.new(2012, 5, 17, 4, 25, 43)))
    end

    it 'should persist a supplier' do
      expect{ Supplier.create(name: 'Test',
                              franchise: Franchise.create(name: "La deuxième franchise de test",
                                                              adress: Adress.create(city: "Test n°45", country: "France", postcode: 00147, street: "Ruelle de test", street_number: 789),
                                                              group: Group.create(name: 'Groupe-test', adress: Adress.create(city: "Ville test", country: "Nord-Testistant", postcode: 36005, street: "Rue de l'essai", street_number: 1)),
                                                              global_rate: 4.1,
                                                              open_date: DateTime.new(2007, 7, 7, 16, 30, 0))
      ) }.to change{ Supplier.count }.by(1)
    end 

    it 'should update a supplier' do
      @supplier = Supplier.first
      @supplier.update(name: 'New name')
  
      expect(@supplier.name).to eq('New name')
    end

    it 'should delete a supplier' do
      supplier = Supplier.last

      expect{ supplier.destroy }.to change{ Supplier.count }.by(-1)
    end

    it 'should read suppliers' do
      suppliers = Supplier.all

      expect(Supplier.count).to eq(suppliers.count)
    end
  end
end
