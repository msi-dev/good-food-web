require 'rails_helper'

RSpec.describe Rank, type: :model do
  context 'CRUD' do
    before do 
      @rank = Rank.create(name: 'test')
    end

    it 'should persist a rank' do
      expect{ Rank.create(name: 'Test') }.to change{ Rank.count }.by(1)
    end 

    it 'should update a rank' do
      @rank = Rank.first
      @rank.update(name: 'New name')
  
      expect(@rank.name).to eq('New name')
    end

    it 'should delete a rank' do
      rank = Rank.last

      expect{ rank.destroy }.to change{ Rank.count }.by(-1)
    end

    it 'should read ranks' do
      ranks = Rank.all

      expect(Rank.count).to eq(ranks.count)
    end
  end

end
