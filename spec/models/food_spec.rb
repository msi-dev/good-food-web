require 'rails_helper'

RSpec.describe Food, type: :model do
  context 'CRUD' do
    before do 
      @food = Food.create(
                    name: "Test",
                    description: "Ceci est un test",
                    price: 16.22,
                    number_consumer: 3,
                    category: Category.create(name: "test")
                )
    end

    it 'should persist a food' do
      expect{ Food.create(
                      name: "Test 2",
                      description: "Ceci est un test 2",
                      price: 10.3,
                      number_consumer: 8,
                      category: Category.create(name: "test 2")
                  ) }.to change{ Food.count }.by(1)
    end 

    it 'should update a food' do
      @food = Food.first
      @food.update(name: 'New name')
  
      expect(@food.name).to eq('New name')
    end

    it 'should delete a food' do
      food = Food.last

      expect{ food.destroy }.to change{ Food.count }.by(-1)
    end

    it 'should read foods' do
      foods = Food.all

      expect(Food.count).to eq(foods.count)
    end
  end
end
