require 'rails_helper'

RSpec.describe TypePromo, type: :model do
  context 'CRUD' do
    before do 
      @type_promo = TypePromo.create(name: 'test')
    end

    it 'should persist a type promo' do
      expect{ TypePromo.create(name: 'Test') }.to change{ TypePromo.count }.by(1)
    end 

    it 'should update a type promo' do
      @type_promo = TypePromo.first
      @type_promo.update(name: 'New name')
  
      expect(@type_promo.name).to eq('New name')
    end

    it 'should delete a type promo' do
      type_promo = TypePromo.last

      expect{ type_promo.destroy }.to change{ TypePromo.count }.by(-1)
    end

    it 'should read type promos' do
      type_promos = TypePromo.all

      expect(TypePromo.count).to eq(type_promos.count)
    end
  end
end
